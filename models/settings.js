'use strict';

const createModel = require('./base/createModel');

const schema = {
  definitions: {},
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: [
    'levelToScale',
    'highestLevel',
    'lowestLevel',
    'minutesPerMeter',
    'defaultPosition',
    'mobileDefaultPosition',
  ],
  properties: {
    levelToScale: {
      $id: '#/properties/levelToScale',
      type: 'array',
      items: {
        $id: '#/properties/levelToScale/items',
        type: 'number',
      },
    },
    highestLevel: {
      $id: '#/properties/highestLevel',
      type: 'integer',
    },
    lowestLevel: {
      $id: '#/properties/lowestLevel',
      type: 'integer',
    },
    minutesPerMeter: {
      $id: '#/properties/minutesPerMeter',
      type: 'number',
    },
    defaultPosition: {
      $id: '#/properties/defaultPosition',
      type: 'object',
      required: ['floor', 'x', 'y', 'level'],
      properties: {
        floor: {
          $id: '#/properties/defaultPosition/properties/floor',
          type: 'string',
          pattern: '',
        },
        x: {
          $id: '#/properties/defaultPosition/properties/x',
          type: 'integer',
        },
        y: {
          $id: '#/properties/defaultPosition/properties/y',
          type: 'integer',
        },
        level: {
          $id: '#/properties/defaultPosition/properties/level',
          type: 'integer',
        },
      },
    },
    mobileDefaultPosition: {
      $id: '#/properties/mobileDefaultPosition',
      type: 'object',
      required: ['floor', 'x', 'y', 'level'],
      properties: {
        floor: {
          $id: '#/properties/mobileDefaultPosition/properties/floor',
          type: 'string',
          pattern: '',
        },
        x: {
          $id: '#/properties/mobileDefaultPosition/properties/x',
          type: 'integer',
        },
        y: {
          $id: '#/properties/mobileDefaultPosition/properties/y',
          type: 'integer',
        },
        level: {
          $id: '#/properties/mobileDefaultPosition/properties/level',
          type: 'integer',
        },
      },
    },
  },
};

const COLLECTION_NAME = 'settings';

module.exports = createModel(COLLECTION_NAME, { schema });
