'use strict';

const successResponse = require('../../responses/successResponse');
const tags = require('../../models/tags');
const getImageUrl = require('./getImageUrl');

async function findTags(req, res) {
  const data = await (await tags.find({}, { projection: { data: 0 } })).toArray();
  res.json(
    successResponse({
      data: data.map(({ hasData, ...item }) => ({
        ...item,
        imageUrl: hasData ? getImageUrl(req, item) : undefined,
      })),
      meta: { count: data.length },
    }),
  );
}

module.exports = findTags;
