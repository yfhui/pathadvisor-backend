'use strict';

const successResponse = require('../../responses/successResponse');
const tags = require('../../models/tags');

async function deleteTag(req, res) {
  const _id = req.params.id;
  await tags.deleteOne({ _id });
  res.json(successResponse({ meta: { deleted: true } }));
}

module.exports = deleteTag;
