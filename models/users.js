'use strict';

const createModel = require('./base/createModel');
const permissions = require('../auth/permissions');

const schema = {
  definitions: {},
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['_id', 'password', 'failedAttempts', 'createdAt', 'updatedAt', 'role'],
  properties: {
    _id: {
      $id: '#/properties/_id',
      type: 'string',
      minLength: 1,
    },
    password: {
      $id: '#/properties/password',
      type: 'string',
      minLength: 1,
    },
    failedAttempts: {
      $id: '#/properties/failedAttempts',
      type: 'integer',
    },
    inactive: {
      $id: '#/properties/inactive',
      type: 'boolean',
    },
    createdAt: {
      $id: '#/properties/createdAt',
      type: 'integer',
    },
    updatedAt: {
      $id: '#/properties/updatedAt',
      type: 'integer',
    },
    lastLoginAt: {
      $id: '#/properties/lastLoginAt',
      type: 'integer',
    },
    role: {
      $id: '#/properties/role',
      type: 'string',
      enum: ['admin', 'user'],
    },
    permissions: {
      $id: '#/properties/permissions',
      type: 'array',
      items: {
        type: 'string',
        enum: Object.values(permissions),
      },
    },
  },
};

const COLLECTION_NAME = 'users';

module.exports = createModel(COLLECTION_NAME, { schema });
