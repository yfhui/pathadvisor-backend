'use strict';

module.exports = function successResponse({ data, meta }) {
  return { data, meta };
};
