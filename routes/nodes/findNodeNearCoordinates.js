'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });

const nodesModel = require('../../models/nodes');
const successResponse = require('../../responses/successResponse');
const pointInPolygon = require('./pointInPolygon');
const ValidationError = require('../../errors/ValidationError');
const transformNodeResponse = require('./transformNodeResponse');

async function findNodeNearCoordinates(req, res) {
  const { floorId } = req.params;
  const { nearCoordinates } = req.query;

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    required: ['floorId', 'nearCoordinates'],
    properties: {
      floorId: {
        $id: '#/properties/floorId',
        type: 'string',
        minLength: 1,
      },
      nearCoordinates: {
        $id: '#/properties/nearCoordinates',
        type: 'string',
        pattern: '^-?[0-9]+,-?[0-9]+$',
      },
    },
  };

  const validate = validator.compile(querySchema);

  if (!validate({ floorId, nearCoordinates })) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  const point = nearCoordinates.split(',').map(v => Number.parseInt(v, 10));

  const MAX_NEAR_DISTANCE = 300;
  const ITEMS_LIMIT = 20;

  const nodes = await (await nodesModel.find(
    {
      floorId,
      coordinates: { $near: point, $maxDistance: MAX_NEAR_DISTANCE },
    },
    { projection: { polygonCoordinates: 0 }, limit: ITEMS_LIMIT },
  )).toArray();

  let node = nodes.find(_node => {
    if (!_node || !_node.geoLocs || !_node.geoLocs.type === 'MultiPolygon') {
      return false;
    }

    return _node.geoLocs.coordinates.some(([outerPolygon]) => pointInPolygon(point, outerPolygon));
  });

  if (!node) {
    // return the nearest node that is a point
    node = nodes.find(_node => _node && (!_node.geoLocs || _node.geoLocs.type !== 'MultiPolygon'));
  }

  res.json(
    successResponse({
      data: node ? transformNodeResponse(req, node) : null,
    }),
  );
}

module.exports = findNodeNearCoordinates;
