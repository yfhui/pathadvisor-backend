'use strict';

const router = require('express-promise-router')();
const findAppVersions = require('./findAppVersions');
const findAppVersionById = require('./findAppVersionById');
const insertAppVersion = require('./insertAppVersion');
const updateAppVersion = require('./updateAppVersion');
const deleteAppVersion = require('./deleteAppVersion');

const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/appVersions', checkPermissions([permissions['APP_VERSION:LIST']]), findAppVersions);
router.get('/appVersions/:id', findAppVersionById);
router.post(
  '/appVersions',
  checkPermissions([permissions['APP_VERSION:INSERT']]),
  insertAppVersion,
);
router.post(
  '/appVersions/:id',
  checkPermissions([permissions['APP_VERSION:UPDATE']]),
  updateAppVersion,
);
router.delete(
  '/appVersions/:id',
  checkPermissions([permissions['APP_VERSION:DELETE']]),
  deleteAppVersion,
);

module.exports = router;
