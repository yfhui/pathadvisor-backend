'use strict';

const Ajv = require('ajv');

const keyBy = require('lodash.keyby');

const validator = new Ajv({ allErrors: true });

const nodes = require('../../models/nodes');
const edges = require('../../models/edges');
const successResponse = require('../../responses/successResponse');
const ValidationError = require('../../errors/ValidationError');

async function findEdgesWithinBox(req, res) {
  const MAX_NODES_RETURN_LIMIT = 400;
  const { floorId } = req.params;
  const { boxCoordinates } = req.query;

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    required: ['floorId', 'boxCoordinates'],
    properties: {
      floorId: {
        $id: '#/properties/floorId',
        type: 'string',
        minLength: 1,
      },
      boxCoordinates: {
        $id: '#/properties/boxCoordinates',
        type: 'string',
        pattern: '^-?[0-9]+,-?[0-9]+,-?[0-9]+,-?[0-9]+$',
      },
    },
  };

  const validate = validator.compile(querySchema);

  if (!validate({ floorId, ...req.query })) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  const [x1, y1, x2, y2] = boxCoordinates.split(',').map(v => Number.parseInt(v, 10));

  const query = { floorId, coordinates: { $geoWithin: { $box: [[x1, y1], [x2, y2]] } } };

  const cursor = await nodes.find(query, {
    limit: MAX_NODES_RETURN_LIMIT,
    projection: { _id: 1, coordinates: 1 },
  });

  const nodeData = await cursor.toArray();

  const nodeIds = nodeData.map(({ _id }) => _id);
  const nodesById = keyBy(nodeData, '_id');

  const edgeCursor = await edges.find({
    $or: [{ fromNodeId: { $in: nodeIds } }, { toNodeId: { $in: nodeIds } }],
  });

  const data = await edgeCursor.toArray();

  const remainingNodeIds = new Set();

  data.forEach(edge => {
    if (!nodesById[edge.fromNodeId]) {
      remainingNodeIds.add(edge.fromNodeId);
    }
    if (!nodesById[edge.toNodeId]) {
      remainingNodeIds.add(edge.toNodeId);
    }
  });

  if (remainingNodeIds.size) {
    const remainingNodes = await (await nodes.find(
      {
        _id: { $in: [...remainingNodeIds] },
      },
      {
        projection: { _id: 1, coordinates: 1 },
      },
    )).toArray();

    remainingNodes.forEach(node => {
      nodesById[node._id] = node;
    });
  }

  res.json(
    successResponse({
      data: data.map(edge => ({
        ...edge,
        fromNodeCoordinates: nodesById[edge.fromNodeId].coordinates,
        toNodeCoordinates: nodesById[edge.toNodeId].coordinates,
      })),
      meta: { count: data.length },
    }),
  );
}

module.exports = findEdgesWithinBox;
