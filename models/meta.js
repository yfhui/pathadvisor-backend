'use strict';

const createModel = require('./base/createModel');

const schema = {
  definitions: {},
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['version'],
  properties: {
    version: {
      $id: '#/properties/version',
      type: 'string',
      minLength: 1,
    },
  },
};

const COLLECTION_NAME = 'meta';

module.exports = createModel(COLLECTION_NAME, { schema });
