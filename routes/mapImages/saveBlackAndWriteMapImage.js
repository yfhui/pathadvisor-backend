'use strict';

const { Binary } = require('mongodb');

const mapImages = require('../../models/mapImages');
const floors = require('../../models/floors');
const { FEATURES } = require('./imageMeta');
const convertBlackAndWriteMapImage = require('./convertBlackAndWriteMapImage');

async function saveBlackAndWriteMapImage(floorId, imageBuffer) {
  const floor = await floors.findOne({ _id: floorId });

  if (!floor) {
    throw new Error('Floor data not found');
  }

  const lastUpdatedAt = new Date().getTime();

  const sharpImage = await convertBlackAndWriteMapImage(imageBuffer);
  await mapImages.insertOne({
    floorId,
    feature: FEATURES.BLACK_AND_WHITE,
    data: new Binary(await sharpImage.png().toBuffer()),
    lastUpdatedAt,
  });
}

module.exports = saveBlackAndWriteMapImage;
