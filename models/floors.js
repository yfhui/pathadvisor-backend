'use strict';

const createModel = require('./base/createModel');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: [
    '_id',
    'buildingId',
    'meterPerPixel',
    'mapWidth',
    'mapHeight',
    'ratio',
    'defaultX',
    'defaultY',
    'defaultLevel',
    'mobileDefaultLevel',
    'mobileDefaultX',
    'mobileDefaultY',
    'startX',
    'startY',
    'rank',
  ],
  properties: {
    _id: {
      $id: '#/properties/_id',
      type: 'string',
      minLength: 1,
    },
    name: {
      $id: '#/properties/name',
      type: 'string',
      minLength: 1,
    },
    buildingId: {
      $id: '#/properties/buildingId',
      type: 'string',
      minLength: 1,
    },
    meterPerPixel: {
      $id: '#/properties/meterPerPixel',
      type: 'number',
    },
    mapWidth: {
      $id: '#/properties/mapWidth',
      type: 'integer',
    },
    mapHeight: {
      $id: '#/properties/mapHeight',
      type: 'integer',
    },
    ratio: {
      $id: '#/properties/ratio',
      type: 'number',
    },
    defaultX: {
      $id: '#/properties/defaultX',
      type: 'integer',
    },
    defaultY: {
      $id: '#/properties/defaultY',
      type: 'integer',
    },
    defaultLevel: {
      $id: '#/properties/defaultLevel',
      type: 'integer',
    },
    mobileDefaultLevel: {
      $id: '#/properties/mobileDefaultLevel',
      type: 'integer',
    },
    mobileDefaultX: {
      $id: '#/properties/mobileDefaultX',
      type: 'integer',
    },
    mobileDefaultY: {
      $id: '#/properties/mobileDefaultY',
      type: 'integer',
    },
    rank: {
      $id: '#/properties/rank',
      type: 'number',
    },
    startX: {
      $id: '#/properties/startX',
      type: 'integer',
    },
    startY: {
      $id: '#/properties/startY',
      type: 'integer',
    },
  },
};

const COLLECTION_NAME = 'floors';

module.exports = createModel(COLLECTION_NAME, { schema });
