'use strict';

const saveAndResizeMapImages = require('./saveAndResizeMapImage');
const saveBlackAndWriteMapImage = require('./saveBlackAndWriteMapImage');
const successResponse = require('../../responses/successResponse');
const ValidationError = require('../../errors/ValidationError');

async function uploadMapImage(req, res) {
  const { floorId } = req.params;

  const imageBuffer = req.body;

  if (!(imageBuffer instanceof Buffer)) {
    throw new ValidationError(
      null,
      'No request body received. Make sure you set content-type to application/octet-stream',
    );
  }

  await saveAndResizeMapImages(floorId, imageBuffer);
  await saveBlackAndWriteMapImage(floorId, imageBuffer);

  res.json(successResponse({ data: { success: true } }));
}

module.exports = uploadMapImage;
