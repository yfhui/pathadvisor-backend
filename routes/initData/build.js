'use strict';

const keyBy = require('lodash.keyby');
const initDataCache = require('./cache');

const buildingModel = require('../../models/buildings');
const floorModel = require('../../models/floors');
const tagModel = require('../../models/tags');
const getImageUrl = require('../tags/getImageUrl');
const settingModel = require('../../models/settings');

async function build(req) {
  const [buildings, floors, tags, settings] = await Promise.all([
    ...[
      buildingModel.find(),
      floorModel.find(),
      tagModel.find({}, { projection: { data: 0 } }),
    ].map(async cursor => (await cursor).toArray()),
    settingModel.findOne({}, { sort: { $natural: -1 } }),
  ]);

  initDataCache.data = {
    floors: keyBy(floors, o => o._id),
    buildingIds: buildings.map(o => o._id),
    buildings: keyBy(
      buildings.map(building => ({
        ...building,
        floorIds: floors
          .filter(floor => floor.buildingId === building._id)
          .sort((a, b) => a.rank - b.rank)
          .map(floor => floor._id),
      })),
      o => o._id,
    ),
    tags: keyBy(
      tags.map(({ hasData, ...tag }) => ({
        ...tag,
        imageUrl: hasData ? getImageUrl(req, tag) : undefined,
      })),
      o => o._id,
    ),
    tagIds: tags.map(o => o._id),
    settings,
  };
}

module.exports = build;
