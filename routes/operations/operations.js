'use strict';

const router = require('express-promise-router')();
const rebuildGraph = require('./rebuildGraph');
const rebuildInitData = require('./rebuildInitData');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');
const getVersion = require('./getVersion');
const getPermissions = require('./getPermissions');

router.post(
  '/graph/rebuild',
  checkPermissions([permissions['OPERATION:GRAPH:REBUILD']]),
  rebuildGraph,
);

router.post(
  '/init-data/rebuild',
  checkPermissions([permissions['OPERATION:INITDATA:REBUILD']]),
  rebuildInitData,
);

router.get('/version', getVersion);
router.get(
  '/permissions',
  checkPermissions([permissions['OPERATION:PERMISSIONS:LIST']]),
  getPermissions,
);
module.exports = router;
