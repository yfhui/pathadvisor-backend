'use strict';

const createModel = require('./base/createModel');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['_id', 'fromNodeId', 'toNodeId', 'weightType', 'floorId'],
  properties: {
    _id: {
      $id: '#/properties/_id',
      type: 'string',
      minLength: 1,
    },
    fromNodeId: {
      $id: '#/properties/fromNodeId',
      type: 'string',
      minLength: 1,
    },
    toNodeId: {
      $id: '#/properties/toNodeId',
      type: 'string',
      minLength: 1,
    },
    weightType: {
      $id: '#/properties/weightType',
      type: 'string',
      enum: ['nodeDistance', 'max', 'number'],
    },
    weight: {
      $id: '#/properties/weight',
      type: 'number',
    },
    floorId: {
      $id: '#/properties/floorId',
      type: 'string',
      minLength: 1,
    },
    notStepFree: {
      $id: '#/properties/notStepFree',
      type: 'boolean',
    },
  },
};

const COLLECTION_NAME = 'edges';

module.exports = createModel(COLLECTION_NAME, { schema });
