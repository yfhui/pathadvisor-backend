'use strict';

const readline = require('readline');
const { addUser } = require('../auth/auth');
const loadEnv = require('../env');

loadEnv();

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function askQuestion(question) {
  return new Promise(resolve => {
    rl.question(question, answer => resolve(answer));
  });
}

async function createUser() {
  const username = await askQuestion('Please enter a username:');
  const password = await askQuestion('Please enter a password:');
  const role = await askQuestion('Please enter a role for this user: [admin or user]');

  let customPermissions = [];
  if (role !== 'admin') {
    customPermissions = await askQuestion('Please enter custom permission, separated by comma');
    customPermissions = customPermissions.split(',');
  }

  try {
    await addUser(username, password, role, customPermissions);
  } catch (error) {
    console.log('Failed to create user.');
    console.log(error);
    rl.close();

    process.exit(1);
  }
  console.log('User created successfully.');
  rl.close();
  process.exit(0);
}

createUser();
