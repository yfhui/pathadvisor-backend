'use strict';

function errorResponse({ code, message, validationErrors, stack }) {
  return {
    error: {
      code,
      message,
      validationErrors,
      stack: process.env.NODE_ENV !== 'production' ? stack : undefined,
    },
  };
}

module.exports = errorResponse;
