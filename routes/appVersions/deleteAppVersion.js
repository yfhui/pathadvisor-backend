'use strict';

const successResponse = require('../../responses/successResponse');
const appVersions = require('../../models/appVersions');

async function deleteAppVersion(req, res) {
  await appVersions.deleteOne({ _id: req.params.id });
  res.json(successResponse({ meta: { deleted: true } }));
}

module.exports = deleteAppVersion;
