'use strict';

module.exports = function compactObject(obj) {
  const compactedObject = {};
  Object.keys(obj).forEach(key => {
    if (!obj[key] || obj[key] === 'null' || (Array.isArray(obj[key]) && obj[key].length === 0)) {
      return;
    }

    compactedObject[key] = obj[key];
  });

  return compactedObject;
};
