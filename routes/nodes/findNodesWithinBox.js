'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });

const keyBy = require('lodash.keyby');
const nodes = require('../../models/nodes');
const connectors = require('../../models/connectors');
const successResponse = require('../../responses/successResponse');
const ValidationError = require('../../errors/ValidationError');
const transformNodeResponse = require('./transformNodeResponse');
const convertLiteralToBoolean = require('../../utils/convertLiteralToBoolean');

async function findNodesWithinBox(req, res) {
  const MAX_NODES_RETURN_LIMIT = 400;
  const { floorId } = req.params;
  const { boxCoordinates } = req.query;
  let { includePoints } = req.query;

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    required: ['floorId', 'boxCoordinates'],
    properties: {
      floorId: {
        $id: '#/properties/floorId',
        type: 'string',
        minLength: 1,
      },
      boxCoordinates: {
        $id: '#/properties/boxCoordinates',
        type: 'string',
        pattern: '^-?[0-9]+,-?[0-9]+,-?[0-9]+,-?[0-9]+$',
      },
      includePoints: {
        $id: '#/properties/includePoints',
        type: 'string',
        enum: ['true', 'false'],
      },
    },
  };

  const validate = validator.compile(querySchema);

  if (!validate({ floorId, ...req.query })) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  includePoints = includePoints ? convertLiteralToBoolean(includePoints) : false;

  const queries = [];

  const [x1, y1, x2, y2] = boxCoordinates.split(',').map(v => Number.parseInt(v, 10));
  const polygonCoordinates = { $geoWithin: { $box: [[x1, y1], [x2, y2]] } };
  queries.push({ floorId, polygonCoordinates });

  if (includePoints) {
    queries.push({ floorId, coordinates: polygonCoordinates });
  }

  const query = queries.length > 1 ? { $or: queries } : queries[0];

  const cursor = await nodes.find(query, {
    limit: MAX_NODES_RETURN_LIMIT,
    projection: { polygonCoordinates: 0 },
  });

  const indexesByConnectorId = {};
  const data = await cursor.toArray();

  data.forEach(({ connectorId }, i) => {
    if (!connectorId) {
      return;
    }

    indexesByConnectorId[connectorId] = indexesByConnectorId[connectorId]
      ? [...indexesByConnectorId[connectorId], i]
      : [i];
  });

  const connectorIds = Object.keys(indexesByConnectorId);

  if (connectorIds.length) {
    const connectorsById = keyBy(
      await (await connectors.find(
        { _id: { $in: connectorIds } },
        {
          projection: { tagIds: 1 },
        },
      )).toArray(),
      o => o._id,
    );

    // Concat tagIds from connectors to nodes
    Object.keys(indexesByConnectorId).forEach(connectorId => {
      const dataIndexes = indexesByConnectorId[connectorId];
      dataIndexes.forEach(dataIndex => {
        data[dataIndex].tagIds = data[dataIndex].tagIds
          ? [...data[dataIndex].tagIds, ...connectorsById[connectorId].tagIds]
          : connectorsById[connectorId].tagIds;
      });
    });
  }

  res.json(
    successResponse({
      data: data.map(node => transformNodeResponse(req, node)),
      meta: { count: data.length },
    }),
  );
}

module.exports = findNodesWithinBox;
