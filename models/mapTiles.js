'use strict';

const createModel = require('./base/createModel');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['floorId', 'x', 'y', 'zoomLevel', 'data', 'lastUpdatedAt'],
  properties: {
    floorId: {
      $id: '#/properties/name',
      type: 'string',
      minLength: 1,
    },
    x: {
      $id: '#/properties/x',
      type: 'integer',
    },
    y: {
      $id: '#/properties/y',
      type: 'integer',
    },
    zoomLevel: {
      $id: '#/properties/zoomLevel',
      type: 'integer',
    },
    data: {
      $id: '#/properties/data',
      type: 'object',
    },
    lastUpdatedAt: {
      $id: '#/properties/lastUpdated',
      type: 'integer',
    },
  },
};

const COLLECTION_NAME = 'mapTiles';

module.exports = createModel(COLLECTION_NAME, { schema });
