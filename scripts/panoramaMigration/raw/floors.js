'use strict';

module.exports = {
  '1': {
    name: '1',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 4516,
    mapHeight: 3291,
    ratio: 0.1,
    defaultX: 2228,
    defaultY: 1499,
    defaultLevel: 3,
    mobileDefaultLevel: 3,
    mobileDefaultX: 2228,
    mobileDefaultY: 1499,
    rank: 1,
    startX: -1412,
    startY: -1386,
    migrateScale: 1.03225806452,
  },
  '2': {
    name: '2',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 4831,
    mapHeight: 3418,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 3476,
    defaultY: 1247,
    mobileDefaultLevel: 3,
    mobileDefaultX: 3476,
    mobileDefaultY: 1247,
    rank: 2,
    startX: -1103,
    startY: -1218,
    migrateScale: 1.03225806452,
  },
  '3': {
    name: '3',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 4680,
    mapHeight: 3111,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 1556,
    defaultY: 849,
    mobileDefaultLevel: 3,
    mobileDefaultX: 1556,
    mobileDefaultY: 849,
    rank: 3,
    startX: -1027,
    startY: -1115,
  },
  '4': {
    name: '4',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 4680,
    mapHeight: 3111,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 1556,
    defaultY: 849,
    mobileDefaultLevel: 3,
    mobileDefaultX: 1556,
    mobileDefaultY: 849,
    rank: 4,
    startX: -1017,
    startY: -1146,
  },
  '5': {
    name: '5',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 4680,
    mapHeight: 3111,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 1556,
    defaultY: 849,
    mobileDefaultLevel: 3,
    mobileDefaultX: 1556,
    mobileDefaultY: 849,
    rank: 5,
    startX: -1024,
    startY: -1180,
  },
  '6': {
    name: '6',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 4680,
    mapHeight: 3111,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 1556,
    defaultY: 849,
    mobileDefaultLevel: 3,
    mobileDefaultX: 1556,
    mobileDefaultY: 849,
    rank: 6,
    startX: -1021,
    startY: -1188,
  },
  '7': {
    name: '7',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 4680,
    mapHeight: 3111,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 1556,
    defaultY: 849,
    mobileDefaultLevel: 3,
    mobileDefaultX: 1556,
    mobileDefaultY: 849,
    rank: 7,
    startX: -1019,
    startY: -1152,
  },
  UC1: {
    name: '1',
    buildingId: 'universityCenter',
    meterPerPixel: 0.0455,
    mapWidth: 2000,
    mapHeight: 1400,
    ratio: 0.15,
    defaultLevel: 3,
    defaultX: 847,
    defaultY: 526,
    mobileDefaultLevel: 3,
    mobileDefaultX: 847,
    mobileDefaultY: 526,
    rank: 1,
  },
  UCG: {
    name: 'G',
    buildingId: 'universityCenter',
    meterPerPixel: 0.0455,
    mapWidth: 2000,
    mapHeight: 1400,
    ratio: 0.15,
    defaultLevel: 3,
    defaultX: 847,
    defaultY: 526,
    mobileDefaultLevel: 3,
    mobileDefaultX: 847,
    mobileDefaultY: 526,
    rank: 0,
  },
  IASG: {
    name: 'G',
    buildingId: 'ias',
    meterPerPixel: 0.0455,
    mapWidth: 1200,
    mapHeight: 1200,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 639,
    defaultY: 575,
    mobileDefaultLevel: 3,
    mobileDefaultX: 639,
    mobileDefaultY: 575,
    rank: 0,
  },
  // IAS1: {
  //   name: '1',
  //   buildingId: 'ias',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 1200,
  //   mapHeight: 1200,
  //   ratio: 0.1,
  //   defaultLevel: 3,
  //   defaultX: 639,
  //   defaultY: 575,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 639,
  //   mobileDefaultY: 575,
  //   rank: 1,
  // },
  // IAS2: {
  //   name: '2',
  //   buildingId: 'ias',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 1200,
  //   mapHeight: 1200,
  //   ratio: 0.1,
  //   defaultLevel: 3,
  //   defaultX: 639,
  //   defaultY: 575,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 639,
  //   mobileDefaultY: 575,
  //   rank: 2,
  // },
  // IAS3: {
  //   name: '3',
  //   buildingId: 'ias',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 1200,
  //   mapHeight: 1200,
  //   ratio: 0.1,
  //   defaultLevel: 3,
  //   defaultX: 639,
  //   defaultY: 575,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 639,
  //   mobileDefaultY: 575,
  //   rank: 3,
  // },
  // IAS4: {
  //   name: '4',
  //   buildingId: 'ias',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 1200,
  //   mapHeight: 1200,
  //   ratio: 0.1,
  //   defaultLevel: 3,
  //   defaultX: 639,
  //   defaultY: 575,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 639,
  //   mobileDefaultY: 575,
  //   rank: 4,
  // },
  // IAS5: {
  //   name: '5',
  //   buildingId: 'ias',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 1200,
  //   mapHeight: 1200,
  //   ratio: 0.1,
  //   defaultLevel: 3,
  //   defaultX: 639,
  //   defaultY: 575,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 639,
  //   mobileDefaultY: 575,
  //   rank: 5,
  // },
  LKSG: {
    name: 'G',
    buildingId: 'LKS',
    meterPerPixel: 0.0455,
    mapWidth: 2600,
    mapHeight: 1600,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 1332,
    defaultY: 913,
    mobileDefaultLevel: 3,
    mobileDefaultX: 1332,
    mobileDefaultY: 913,
    rank: 0,
  },
  // LKS1: {
  //   name: '1',
  //   buildingId: 'LKS',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2600,
  //   mapHeight: 1600,
  //   ratio: 0.1,
  //   defaultLevel: 3,
  //   defaultX: 1089,
  //   defaultY: 537,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1089,
  //   mobileDefaultY: 537,
  //   rank: 1,
  // },
  // LKS2: {
  //   name: '2',
  //   buildingId: 'LKS',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2600,
  //   mapHeight: 1600,
  //   ratio: 0.1,
  //   defaultLevel: 3,
  //   defaultX: 1089,
  //   defaultY: 537,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1089,
  //   mobileDefaultY: 537,
  //   rank: 2,
  // },
  // LKS3: {
  //   name: '3',
  //   buildingId: 'LKS',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2600,
  //   mapHeight: 1600,
  //   ratio: 0.1,
  //   defaultLevel: 3,
  //   defaultX: 1089,
  //   defaultY: 537,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1089,
  //   mobileDefaultY: 537,
  //   rank: 3,
  // },
  // LKS4: {
  //   name: '4',
  //   buildingId: 'LKS',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2600,
  //   mapHeight: 1600,
  //   ratio: 0.1,
  //   defaultLevel: 3,
  //   defaultX: 1089,
  //   defaultY: 537,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1089,
  //   mobileDefaultY: 537,
  //   rank: 4,
  // },
  // LKS5: {
  //   name: '5',
  //   buildingId: 'LKS',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2600,
  //   mapHeight: 1600,
  //   ratio: 0.1,
  //   defaultLevel: 3,
  //   defaultX: 562,
  //   defaultY: 437,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 562,
  //   mobileDefaultY: 437,
  //   rank: 5,
  // },
  // LKS6: {
  //   name: '6',
  //   buildingId: 'LKS',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2600,
  //   mapHeight: 1600,
  //   ratio: 0.1,
  //   defaultLevel: 3,
  //   defaultX: 1089,
  //   defaultY: 537,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1089,
  //   mobileDefaultY: 537,
  //   rank: 6,
  // },
  // LKS7: {
  //   name: '7',
  //   buildingId: 'LKS',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2600,
  //   mapHeight: 1600,
  //   ratio: 0.1,
  //   defaultLevel: 3,
  //   defaultX: 1089,
  //   defaultY: 537,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1089,
  //   mobileDefaultY: 537,
  //   rank: 7,
  // },
  // CYTG: {
  //   name: 'G',
  //   buildingId: 'cyt',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2000,
  //   mapHeight: 1400,
  //   ratio: 0.15,
  //   defaultLevel: 3,
  //   defaultX: 1001,
  //   defaultY: 593,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1001,
  //   mobileDefaultY: 593,
  //   rank: 0,
  // },
  // CYTUG: {
  //   name: 'UG',
  //   buildingId: 'cyt',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2000,
  //   mapHeight: 1400,
  //   ratio: 0.15,
  //   defaultLevel: 3,
  //   defaultX: 1001,
  //   defaultY: 593,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1001,
  //   mobileDefaultY: 593,
  //   rank: 0.5,
  // },
  // CYT1: {
  //   name: '1',
  //   buildingId: 'cyt',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2000,
  //   mapHeight: 1400,
  //   ratio: 0.15,
  //   defaultLevel: 3,
  //   defaultX: 1001,
  //   defaultY: 593,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1001,
  //   mobileDefaultY: 593,
  //   rank: 1,
  // },
  // CYT2: {
  //   name: '2',
  //   buildingId: 'cyt',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2000,
  //   mapHeight: 1400,
  //   ratio: 0.15,
  //   defaultLevel: 3,
  //   defaultX: 1001,
  //   defaultY: 593,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1001,
  //   mobileDefaultY: 593,
  //   rank: 2,
  // },
  // CYT3: {
  //   name: '3',
  //   buildingId: 'cyt',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2000,
  //   mapHeight: 1400,
  //   ratio: 0.15,
  //   defaultLevel: 3,
  //   defaultX: 1001,
  //   defaultY: 593,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1001,
  //   mobileDefaultY: 593,
  //   rank: 3,
  // },
  // CYT4: {
  //   name: '4',
  //   buildingId: 'cyt',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2000,
  //   mapHeight: 1400,
  //   ratio: 0.15,
  //   defaultLevel: 3,
  //   defaultX: 1001,
  //   defaultY: 593,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1001,
  //   mobileDefaultY: 593,
  //   rank: 4,
  // },
  // CYT5: {
  //   name: '5',
  //   buildingId: 'cyt',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2000,
  //   mapHeight: 1400,
  //   ratio: 0.15,
  //   defaultLevel: 3,
  //   defaultX: 1001,
  //   defaultY: 593,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1001,
  //   mobileDefaultY: 593,
  //   rank: 5,
  // },
  // CYT6: {
  //   name: '6',
  //   buildingId: 'cyt',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2000,
  //   mapHeight: 1400,
  //   ratio: 0.15,
  //   defaultLevel: 3,
  //   defaultX: 1001,
  //   defaultY: 593,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1001,
  //   mobileDefaultY: 593,
  //   rank: 6,
  // },
  // CYT7: {
  //   name: '7',
  //   buildingId: 'cyt',
  //   meterPerPixel: 0.0455,
  //   mapWidth: 2000,
  //   mapHeight: 1400,
  //   ratio: 0.15,
  //   defaultLevel: 3,
  //   defaultX: 1001,
  //   defaultY: 593,
  //   mobileDefaultLevel: 3,
  //   mobileDefaultX: 1001,
  //   mobileDefaultY: 593,
  //   rank: 7,
  // },
  Overall: {
    buildingId: 'campusMap',
    meterPerPixel: 0.413,
    mapWidth: 2200,
    mapHeight: 1800,
    ratio: 0.1,
    defaultX: 936,
    defaultY: 654,
    defaultLevel: 3,
    mobileDefaultLevel: 3,
    mobileDefaultX: 936,
    mobileDefaultY: 654,
    rank: 0,
  },
  G: {
    name: 'G',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 4992,
    mapHeight: 3532,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 2162,
    defaultY: 1263,
    mobileDefaultLevel: 3,
    mobileDefaultX: 2162,
    mobileDefaultY: 1263,
    rank: 0,
    startX: -2303,
    startY: -1271,
    migrateScale: 1.06666666667,
  },
  LG1: {
    name: 'LG1',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 4237,
    mapHeight: 3047,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 2419,
    defaultY: 962,
    mobileDefaultLevel: 3,
    mobileDefaultX: 2419,
    mobileDefaultY: 962,
    rank: -1,
    startX: -2420,
    startY: -1243,
  },
  LG3: {
    name: 'LG3',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 3361,
    mapHeight: 2413,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 2398,
    defaultY: 486,
    mobileDefaultLevel: 3,
    mobileDefaultX: 2398,
    mobileDefaultY: 486,
    rank: -3,
    startX: -2409,
    startY: -788,
  },
  LG4: {
    name: 'LG4',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 3194,
    mapHeight: 2311,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 2344,
    defaultY: 577,
    mobileDefaultLevel: 3,
    mobileDefaultX: 2344,
    mobileDefaultY: 577,
    rank: -4,
    startX: -2337,
    startY: -840,
  },
  LG5: {
    name: 'LG5',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 3511,
    mapHeight: 2573,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 1901,
    defaultY: 590,
    mobileDefaultLevel: 3,
    mobileDefaultX: 1901,
    mobileDefaultY: 590,
    rank: -5,
    migrateScale: 1.14285714286,
    startX: -2929,
    startY: -802,
  },
  LG7: {
    name: 'LG7',
    buildingId: 'academicBuilding',
    meterPerPixel: 0.071875,
    mapWidth: 3171,
    mapHeight: 1643,
    ratio: 0.1,
    defaultLevel: 3,
    defaultX: 1833,
    defaultY: 435,
    mobileDefaultLevel: 3,
    mobileDefaultX: 1833,
    mobileDefaultY: 435,
    rank: -7,
    migrateScale: 1.03225806452,
    startX: -2779,
    startY: -937,
  },
};
