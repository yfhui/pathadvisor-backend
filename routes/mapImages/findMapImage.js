'use strict';

const Ajv = require('ajv');

const ValidationError = require('../../errors/ValidationError');
const mapImages = require('../../models/mapImages');
const NotFoundError = require('../../errors/NotFoundError');

const validator = new Ajv({ allErrors: true });

async function findMapImage(req, res) {
  const { floorId } = req.params;

  const querySchema = {
    definitions: {},
    additionalProperties: false,
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    properties: {
      feature: {
        $id: '#/properties/feature',
        type: ['string', 'null'],
        minLength: 1,
      },
    },
  };

  const validate = validator.compile(querySchema);
  const { feature = null } = req.query;

  if (!validate({ feature })) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  const query = { floorId, feature };

  const [data] = await (await mapImages.find(query, {
    limit: 1,
    sort: { lastUpdatedAt: -1 },
  })).toArray();

  if (!data) {
    throw new NotFoundError(query, 'Map image not found');
  }
  if (!data.data || !data.data.buffer) {
    throw new NotFoundError(query, 'Image buffer not found in database');
  }

  res.set('Content-Type', 'image/png');
  res.send(data.data.buffer);
}

module.exports = findMapImage;
