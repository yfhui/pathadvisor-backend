'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });

const nodeModel = require('../../models/nodes');
const panoEdgeModel = require('../../models/panoEdges');
const successResponse = require('../../responses/successResponse');
const transformNodeResponse = require('./transformNodeResponse');
const ValidationError = require('../../errors/ValidationError');

async function findNextPanoNode(req, res) {
  const MAX_ANGLE_DIFFERENCE = 45;

  let { forwardAngle } = req.query;
  const { startId } = req.query;
  forwardAngle = parseInt(forwardAngle, 10);

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    required: ['startId', 'forwardAngle'],
    properties: {
      startId: {
        $id: '#/properties/startId',
        type: 'string',
        minLength: 1,
      },
      forwardAngle: {
        $id: '#/properties/forwardAngle',
        type: 'integer',
        minimum: 0,
        maximum: 360,
      },
    },
  };

  const validate = validator.compile(querySchema);

  if (!validate({ startId, forwardAngle })) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  const {
    coordinates: [currX, currY],
  } = await nodeModel.findOne({ _id: startId }, { projection: { polygonCoordinates: 0 } });

  const panoEdges = await (await panoEdgeModel.find({ fromNodeId: startId })).toArray();

  let node;
  let minimumAngleDifference = 360;

  for (const panoEdge of panoEdges) {
    const toNode = await nodeModel.findOne(
      { _id: panoEdge.toNodeId },
      { projection: { polygonCoordinates: 0 } },
    );

    const [neighborX, neighborY] = toNode.coordinates;
    const [dx, dy] = [neighborX - currX, neighborY - currY];

    /* In Map Coordinate System, y+ is pointing west and x+ is pointing south. */
    /* The formula below calculates the relative angle of neighbor node to current location, measured from north direction oriented clockwise. */
    const relativeAngle =
      180 +
      ((Math.sign(dy + 0.000001) * Math.acos(dx / Math.sqrt(dx * dx + dy * dy + 0.000001))) /
        Math.PI) *
        180;

    /* The difference between target point relativeAngle and forwardAngle should be the less in diff and 360 - diff */
    let diff = Math.abs(relativeAngle - forwardAngle);
    diff = Math.min(diff, 360 - diff);

    if (diff < minimumAngleDifference) {
      minimumAngleDifference = diff;
      node = toNode;
    }
  }

  if (minimumAngleDifference > MAX_ANGLE_DIFFERENCE) {
    node = null;
  }

  res.json(
    successResponse({
      data: node ? transformNodeResponse(req, node) : null,
    }),
  );
}

module.exports = findNextPanoNode;
