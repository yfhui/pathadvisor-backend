'use strict';

const createModel = require('./base/createModel');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['_id', 'nodeId'],
  properties: {
    _id: {
      $id: '#/properties/_id',
      type: 'string',
      minLength: 1,
    },
    others: {
      $id: '#/properties/others',
      type: 'object',
    },
  },
};

const COLLECTION_NAME = 'nodeOthers';

module.exports = createModel(COLLECTION_NAME, { schema });
