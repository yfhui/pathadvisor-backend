'use strict';

const bodyParser = require('body-parser');
const router = require('express-promise-router')();
const findImage = require('./findImage');
const uploadImage = require('./uploadImage');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/images/:id', findImage);
router.post(
  '/images',
  checkPermissions([permissions['IMAGE:INSERT']]),
  bodyParser.raw({ limit: '2mb' }),
  uploadImage,
);

module.exports = router;
