'use strict';

const router = require('express-promise-router')();

const login = require('./login');
const logout = require('./logout');
const isLoggedIn = require('./isLoggedIn');

router.post('/login', login);
router.post('/logout', logout);
router.get('/is-logged-in', isLoggedIn);

module.exports = router;
