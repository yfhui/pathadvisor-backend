'use strict';

const keyBy = require('lodash.keyby');
const groupBy = require('lodash.groupby');
const nodeModel = require('../models/nodes');
const edgeModel = require('../models/edges');
const connectorModel = require('../models/connectors');
const floorModel = require('../models/floors');
const settingModel = require('../models/settings');

const logger = require('../logger/logger');

const { MAX_WEIGHT, CONNECTOR_SMALLEST_WEIGHT } = require('./constants');

function getDistance([x1, y1], [x2, y2]) {
  return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5;
}

async function build() {
  logger.info('Building Graph...');

  const graph = {};

  const [nodes, edges, connectors, floors, settingsList] = await Promise.all(
    [
      nodeModel.find(
        {},
        {
          projection: {
            coordinates: 1,
            floorId: 1,
            tagIds: 1,
            connectorId: 1,
          },
        },
      ),
      edgeModel.find(),
      connectorModel.find(),
      floorModel.find(),
      settingModel.find({}, { limit: 1, sort: { $natural: -1 } }),
    ].map(async cursor => (await cursor).toArray()),
  );

  const connectorsById = keyBy(connectors, o => o._id);
  nodes.forEach(({ tagIds, connectorId }, index) => {
    if (connectorId && !connectorsById[connectorId]) {
      throw new Error(`Connector ${connectorId} not found`);
    }

    nodes[index].tags = keyBy(tagIds);

    if (connectorId) {
      nodes[index].tags = { ...nodes[index].tags, ...keyBy(connectorsById[connectorId].tagIds) };
    }
  });

  const nodesById = keyBy(nodes, o => o._id);
  const nodeWithConnector = nodes.filter(({ connectorId }) => connectorId);
  const nodesByConnectorId = groupBy(nodeWithConnector, o => o.connectorId);
  const floorsById = keyBy(floors, o => o._id);
  const settings = settingsList[0];

  if (!settings) {
    throw new Error('No settings found');
  }

  edges.forEach(({ _id, fromNodeId, toNodeId, weightType, weight, notStepFree = false }) => {
    let calculatedWeight = 0;
    const nodeFloorId = nodesById[toNodeId].floorId;

    switch (weightType) {
      case 'number':
        calculatedWeight = weight;
        break;

      case 'nodeDistance':
        if (!nodesById[fromNodeId]) {
          throw new Error(`Node ${fromNodeId} not found`);
        }

        if (!nodesById[toNodeId]) {
          throw new Error(`Node ${toNodeId} not found`);
        }

        calculatedWeight =
          getDistance(nodesById[fromNodeId].coordinates, nodesById[toNodeId].coordinates) *
          floorsById[nodeFloorId].meterPerPixel;
        break;

      case 'max':
        calculatedWeight = MAX_WEIGHT;
        break;

      default:
        throw new Error(`Invalid weightType ${weightType} for edge ${_id}`);
    }

    if (!graph[fromNodeId]) {
      graph[fromNodeId] = [];
    }

    graph[fromNodeId].push([toNodeId, calculatedWeight, false, notStepFree]);
  });

  // Add edges for connectors
  nodeWithConnector.forEach(({ _id, connectorId }) => {
    const connector = connectorsById[connectorId];

    if (!connector) {
      throw new Error(`Connector ${connectorId} not found`);
    }

    if (!nodesByConnectorId[connectorId].length) {
      throw new Error(`No nodes connecting to ${connectorId}`);
    }

    if (!graph[_id]) {
      graph[_id] = [];
    }

    nodesByConnectorId[connectorId].forEach(({ _id: toNodeId }) => {
      if (toNodeId === _id) {
        return;
      }

      const notStepFree = connector.tagIds.some(tagId => ['stair', 'escalator'].includes(tagId));
      graph[_id].push([
        toNodeId,
        (connector.weight && connector.weight / settings.minutesPerMeter) ||
          CONNECTOR_SMALLEST_WEIGHT,
        true,
        notStepFree,
      ]);
    });
  });

  logger.info('Graph built');
  return { graph, nodesById, connectorsById, settings };
}

module.exports = build;
