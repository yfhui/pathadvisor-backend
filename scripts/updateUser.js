'use strict';

const readline = require('readline');
const users = require('../models/users');
const loadEnv = require('../env');

loadEnv();

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function askQuestion(question) {
  return new Promise(resolve => {
    rl.question(question, answer => resolve(answer));
  });
}

const ACTIONS = {
  LOCK: 'LOCK',
  UNLOCK: 'UNLOCK',
};

async function update() {
  const username = await askQuestion('Please enter a username:');
  let action = '';

  while (!ACTIONS[action]) {
    // eslint-disable-next-line no-await-in-loop
    action = await askQuestion(`Please enter an action [${Object.values(ACTIONS)}]:`);
    action = action.toUpperCase();
  }

  try {
    let data;

    if (action === ACTIONS.LOCK) {
      data = { inactive: true };
    } else if (action === ACTIONS.UNLOCK) {
      data = { inactive: false, failedAttempts: 0 };
    }

    await users.updateOne({ _id: username }, data);
  } catch (error) {
    console.log('Failed to update user.');
    console.log(error);
    rl.close();

    process.exit(1);
  }
  console.log('User updated successfully.');
  rl.close();
  process.exit(0);
}

update();
