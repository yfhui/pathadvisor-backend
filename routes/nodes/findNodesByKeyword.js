'use strict';

const keyBy = require('lodash.keyby');
const nodes = require('../../models/nodes');
const connectors = require('../../models/connectors');
const errorResponse = require('../../responses/errorResponse');
const successResponse = require('../../responses/successResponse');
const nodeKeywordQuery = require('./nodeKeywordQuery');
const transformNodeResponse = require('./transformNodeResponse');

async function findNodesByKeyword(req, res) {
  const MAX_NODES_RETURN_LIMIT = 10;

  const { name = '' } = req.query || {};
  const query = nodeKeywordQuery(name);

  if (Object.keys(query).length === 0) {
    res.status(400).json(
      errorResponse({
        message: 'Empty query is not allowed',
      }),
    );

    return;
  }

  query.unsearchable = null;

  const cursor = await nodes.find(query, {
    limit: MAX_NODES_RETURN_LIMIT,
    projection: { polygonCoordinates: 0 },
  });

  const data = await cursor.toArray();
  const connectorIds = data.map(({ connectorId }) => connectorId);

  const connectorsById = connectorIds
    ? keyBy(
        await (await connectors.find(
          { _id: { $in: connectorIds } },
          {
            projection: { tagIds: 1 },
          },
        )).toArray(),
        o => o._id,
      )
    : {};

  const filteredData = data.filter(
    ({ connectorId }) =>
      !connectorId ||
      // skip crossBuildingConnector/stairs/escalator in auto complete search result
      !['crossBuildingConnector', 'stair', 'escalator'].some(tag =>
        (connectorsById[connectorId].tagIds || []).includes(tag),
      ),
  );

  res.json(
    successResponse({
      data: filteredData.map(node => transformNodeResponse(req, node)),
      meta: { count: filteredData.length },
    }),
  );
}

module.exports = findNodesByKeyword;
