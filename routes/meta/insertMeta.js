'use strict';

const successResponse = require('../../responses/successResponse');
const meta = require('../../models/meta');

async function insertMeta(req, res) {
  const timestamp = new Date().toISOString();
  const data = await meta.insertOne({
    version: timestamp,
  });
  res.json(successResponse({ data }));
}

module.exports = insertMeta;
