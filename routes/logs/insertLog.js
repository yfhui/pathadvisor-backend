'use strict';

const UAParser = require('ua-parser-js');
const successResponse = require('../../responses/successResponse');
const logs = require('../../models/logs');

async function insertLog(req, res) {
  const data = await logs.insertOne({
    ...req.body,
    datetime: new Date().getTime(),
    ipAddress: req.headers['x-forwarded-for'] || req.ip,
    referer: req.header('referer'),
    userAgent: UAParser(req.header('user-agent')),
  });

  if (process.env.NODE_ENV === 'production') {
    res.json(successResponse({ data: { success: true } }));
    return;
  }

  res.json(successResponse({ data }));
}

module.exports = insertLog;
