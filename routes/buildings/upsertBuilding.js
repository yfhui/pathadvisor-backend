'use strict';

const successResponse = require('../../responses/successResponse');
const buildings = require('../../models/buildings');
const NotFoundError = require('../../errors/NotFoundError');

async function upsertBuilding(req, res) {
  const _id = req.params.id;

  try {
    const data = await buildings.updateOne({ _id }, req.body);
    res.json(successResponse({ data }));
  } catch (error) {
    if (!(error instanceof NotFoundError)) {
      throw error;
    }

    // insert new data if not found one
    const data = await buildings.insertOne({ ...req.body, _id });
    res.json(successResponse({ data }));
  }
}

module.exports = upsertBuilding;
