'use strict';

const permissions = require('./permissions');

const roles = {
  admin: {
    permissions,
  },
  user: {
    permissions: [],
  },
};

module.exports = roles;
