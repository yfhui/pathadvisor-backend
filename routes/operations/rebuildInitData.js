'use strict';

const successResponse = require('../../responses/successResponse');
const build = require('../initData/build');

async function rebuildInitData(req, res) {
  await build(req);
  res.json(successResponse({ data: { success: true } }));
}

module.exports = rebuildInitData;
