'use strict';

const successResponse = require('../../responses/successResponse');
const apiKeys = require('../../models/apiKeys');
const ObjectId = require('../../utils/ObjectId');

async function updateApiKey(req, res) {
  const _id = ObjectId(req.params.id);
  const timestamp = new Date().getTime();

  const { key, createdAt, lastUsedAt, ...rest } = req.body;

  const data = await apiKeys.updateOne(
    { _id },
    {
      ...rest,
      updatedAt: timestamp,
    },
  );

  res.json(successResponse({ data }));
}

module.exports = updateApiKey;
