'use strict';

const pick = require('lodash.pick');
const successResponse = require('../../responses/successResponse');
const tags = require('../../models/tags');
const NotFoundError = require('../../errors/NotFoundError');

async function upsertTag(req, res) {
  const _id = req.params.id;
  const sanitizedBody = pick(req.body, ['name']);

  try {
    const data = await tags.updateOne({ _id }, sanitizedBody);
    res.json(successResponse({ data }));
  } catch (error) {
    if (!(error instanceof NotFoundError)) {
      throw error;
    }

    // insert new data if not found one
    const data = await tags.insertOne({ ...sanitizedBody, _id });
    res.json(successResponse({ data }));
  }
}

module.exports = upsertTag;
