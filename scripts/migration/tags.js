'use strict';

const { RAW_DATA_PATH } = process.env;
const fs = require('fs');
const Binary = require('mongodb').Binary;
const tags = require('../../models/tags');

// eslint-disable-next-line
const tagsData = require(`${process.cwd()}/${RAW_DATA_PATH}/tags.js`);

async function migrate() {
  await Promise.all(
    tagsData.map(async ({ id, imgUrl, ...others }) => {
      try {
        const binData = imgUrl
          ? {
              hasData: true,
              data: new Binary(fs.readFileSync(`${process.cwd()}/${RAW_DATA_PATH}/${imgUrl}`)),
            }
          : {};
        await tags.insertOne({ ...others, _id: id, ...binData });
        console.log(`Inserted tag with _id ${id}`);
      } catch (err) {
        console.error(`Error inserting floor with _id ${id}`, err);
      }
    }),
  );

  console.log('Done');
  process.exit();
}

migrate();
