# Underlying MongoDB

The backend requires a underlying MongoDB instance. The production version DB is hidden from the public. To debug the program, you may deploy a database instace on your local machine.

## Installing

You can download a dump of the DB [here](https://hkustconnect-my.sharepoint.com/:f:/g/personal/zhuangbi_connect_ust_hk/ElSagG9VpDdAqbUCwu1P9B0BLub7_6KNIqXUOsUjgi2a2g?e=fblr5b). (access requires logging in with the university account.) 

The "vanilla" version is the database *before* street view feature is added. The "streetv" version is the database *with street view* feature. You can use the vanilla version to test the migration scripts.

Also you need to install [mongoDB tools](https://www.mongodb.com/download-center/community).

## Deploying

Create a folder where your local database stores files, e.g. `C:/users/<username>/Documents/pathadvisor-mongodb`. Let's call it `folder` for simplicity.

Run

```shell
mongod --dbpath=folder
```

This will start a MongoDB service at `localhost:27017` which read/write files in `folder`. At the first time, many files are created inside the folder as you can check. The shell process will persist with no output, no logging, which is wired but perfectly normal. You can stop the service by `Ctrl+C`, but we must keep it running when testing the program. So **keep this shell running and open another shell**.

To test the status of the DB, in another shell run

```shell
mongo
```

This will try to connect the database at `localhost:27017` since no URL is specified. The connection should be successful, and you are now in the mongo shell, where you can type

```shell
> db
# test
```

to see the current database name. It is "test" by default. And we are going to restore the dump to this database. For now, exit the mongo shell by `Ctrl+C`.

Next, extract the archive. Say the content is now `<somewhere>/dump-<xxx>/test/<.bson and .metadata.json files>`. Run

```shell
mongorestore <somewhere>/dump-<xxx>
```

This will create a database called `test` (base on the sub-folder name) and import all collections which you can check in mongo shell.

```shell
$ mongo

> show dbs
# admin                0.000GB
# config               0.000GB
# local                0.000GB
# test                 0.001GB

> db.getCollectionNames()

# [
#   "buildings",
#   "connectors",
#   "edges",
#   "floors",
#   "images",
#   "mapImages",
#   "mapTiles",
#   "meta",
#   "nodes",
#   "settings",
#   "suggestions",
#   "tags"
# ]

> exit # or Ctrl+C
# bye

$
```

Now the database is restored. The backend will work as long as the MongoDB service is not terminated. 

**In addition, when you debug the program next time, there is no need to re-restore.** Just run and keep the `mongod` process.

```shell
mongod --dbpath=folder
```

## Environment Variables

To associate the backend program with the underlying MongoDB, you need to create a `.env` at this project root with 

```env
DB_PATH=mongodb://127.0.0.1:27017/test
```

As you can see, the URL contains the host, port and database name.

More `.env` fields are listed in the [main README](README.md)
