'use strict';

const successResponse = require('../../responses/successResponse');
const floors = require('../../models/floors');

async function findFloors(req, res) {
  const data = await floors.findOne({ _id: req.params.id });
  res.json(successResponse({ data }));
}

module.exports = findFloors;
