'use strict';

function nodeKeywordQuery(name = '') {
  // eslint-disable-next-line no-param-reassign
  name = name.trim();
  const query = {};

  if (name) {
    query.$or = [
      {
        name: {
          $regex: name,
          $options: 'i',
        },
      },
      {
        keywords: {
          $regex: name,
          $options: 'i',
        },
      },
    ];
  }
  return query;
}

module.exports = nodeKeywordQuery;
