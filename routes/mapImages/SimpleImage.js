'use strict';

class SimpleImage {
  constructor(buffet, info) {
    this.buffet = buffet;
    this.info = info;
  }

  getPixel(x, y) {
    const { channels, width } = this.info;
    const start = y * width * channels + x * channels;
    return this.buffet.slice(start, start + channels);
  }

  setPixel(x, y, color) {
    const { channels, width } = this.info;
    const start = y * width * channels + x * channels;
    const { r, g, b } = color;
    [r, g, b].forEach((val, offset) => {
      this.buffet.writeUInt8(val, start + offset);
    });
  }
}

module.exports = SimpleImage;
