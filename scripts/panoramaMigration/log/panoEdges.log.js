'use strict';

const winston = require('winston');

const logger = winston.createLogger({
  format: winston.format.json(),
  transports: [
    new winston.transports.File({
      filename: `${__dirname}/panoEdges.verbose.log`,
      level: 'verbose',
    }),
    new winston.transports.File({
      filename: `${__dirname}/panoEdges.warn.log`,
      level: 'warn',
    }),
    new winston.transports.Console({
      level: 'info',
      format: winston.format.cli(),
    }),
  ],
});

module.exports = logger;
