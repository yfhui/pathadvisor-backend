'use strict';

const successResponse = require('../../responses/successResponse');
const apiKeys = require('../../models/apiKeys');
const { randomBytes } = require('../../auth/auth');

async function insertApiKey(req, res) {
  const key = await randomBytes();
  const timestamp = new Date().getTime();
  const data = await apiKeys.insertOne({
    ...req.body,
    key,
    createdAt: timestamp,
    updatedAt: timestamp,
  });

  res.json(successResponse({ data }));
}

module.exports = insertApiKey;
