'use strict';

const Ajv = require('ajv');
const successResponse = require('../../responses/successResponse');
const ValidationError = require('../../errors/ValidationError');
const suggestions = require('../../models/suggestions');
const ObjectId = require('../../utils/ObjectId');

async function resolveSuggestion(req, res) {
  const requestBodySchema = {
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'boolean',
  };

  const validator = new Ajv({ allErrors: true });
  const validate = validator.compile(requestBodySchema);

  if (!validate(req.body)) {
    throw new ValidationError(validate.errors, 'Request body validation errors');
  }

  const data = await suggestions.updateOne(
    { _id: new ObjectId(req.params.id) },
    { resolved: req.body, updatedAt: new Date().getTime() },
  );
  res.json(successResponse({ data }));
}

module.exports = resolveSuggestion;
