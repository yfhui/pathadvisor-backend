'use strict';

const crypto = require('crypto');
const bcrypt = require('bcrypt');
const keyBy = require('lodash.keyby');
const users = require('../models/users');
const sessions = require('../models/sessions');
const apiKeys = require('../models/apiKeys');
const roles = require('../auth/roles');

const SALT_ROUND = 10;
const MAX_FAILED_ATTEMPTS = 5;
const REASONS = {
  INVALID_USERNAME: 'INVALID_USERNAME',
  INVALID_PASSWORD: 'INVALID_PASSWORD',
  LOCKED: 'LOCKED',
  MAX_SESSION_REACHED: 'MAX_SESSION_REACHED',
};
const RANDOM_BYTE_LENGTH = 64;
const MAX_SESSION_PER_USER = 5;
const SESSION_TIMEOUT_SINCE_LAST_ACTIVITY = 3600;
const API_KEY_HEADER = 'pathadvisor-api-key';
const SESSION_KEY_HEADER = 'pathadvisor-session-key';

async function addUser(username, plainPassword, role, customPermissions) {
  const timestamp = new Date().getTime();
  const password = await bcrypt.hash(plainPassword, SALT_ROUND);

  return users.insertOne({
    _id: username,
    password,
    role,
    createdAt: timestamp,
    updatedAt: timestamp,
    failedAttempts: 0,
    ...(customPermissions.length ? { permissions: customPermissions } : {}),
  });
}

async function authenticateUser(username, plainPassword) {
  const user = await users.findOne({ _id: username });

  if (!user) {
    return {
      error: REASONS.INVALID_USERNAME,
    };
  }

  if (user.failedAttempts >= MAX_FAILED_ATTEMPTS || user.inactive) {
    return {
      error: REASONS.LOCKED,
    };
  }

  if (!(await bcrypt.compare(plainPassword, user.password))) {
    return {
      error: REASONS.INVALID_PASSWORD,
    };
  }

  return {};
}

function randomBytes() {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(RANDOM_BYTE_LENGTH, (err, buffer) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(buffer.toString('hex'));
    });
  });
}

async function createSession(username) {
  const session = await randomBytes();
  const timestamp = new Date().getTime();

  const currentSessions = await (await sessions.find(
    { username, lastUsedAt: { $gte: timestamp - SESSION_TIMEOUT_SINCE_LAST_ACTIVITY * 1000 } },
    { projection: { _id: 1 } },
  )).toArray();

  if (currentSessions.length >= MAX_SESSION_PER_USER) {
    return { error: REASONS.MAX_SESSION_REACHED };
  }

  await sessions.insertOne({ _id: session, username, lastUsedAt: timestamp });
  return { session };
}

async function getPermissionsFromSession(sessionKey) {
  const timestamp = new Date().getTime();

  const session = await sessions.findOne(
    {
      _id: sessionKey,
      lastUsedAt: { $gte: timestamp - SESSION_TIMEOUT_SINCE_LAST_ACTIVITY * 1000 },
    },
    { projection: { username: 1 } },
  );

  if (!session) {
    return { error: 'No session found' };
  }

  const user = await users.findOne(
    { _id: session.username },
    { projection: { inactive: 1, role: 1, permissions: 1 } },
  );

  if (!user || user.inactive) {
    return { error: 'No user found' };
  }

  await sessions.updateOne({ _id: sessionKey }, { lastUsedAt: timestamp });

  const role = user.role;

  const rolePermissions = roles[role] && roles[role].permissions;

  if (!rolePermissions) {
    return { error: `Invalid role ${role}` };
  }

  const customPermissions = user.permissions
    ? user.permissions.reduce((agg, v) => ({ ...agg, [v]: v }), {})
    : {};

  return { permissions: { ...rolePermissions, ...customPermissions } };
}

async function getPermissionsFromApiKey(key) {
  const timestamp = new Date().getTime();

  const apiKey = await apiKeys.findOne(
    { key },
    { projection: { inactive: 1, permissions: 1, namespace: 1, expiryAt: 1 } },
  );

  if (!apiKey || apiKey.inactive || (apiKey.expiryAt && apiKey.expiryAt < timestamp)) {
    return { error: 'No active key found' };
  }

  await apiKeys.updateOne({ key }, { lastUsedAt: timestamp });

  return {
    permissions: keyBy(apiKey.permissions),
    namespace: apiKey.namespace,
  };
}

module.exports = {
  addUser,
  authenticateUser,
  SALT_ROUND,
  MAX_FAILED_ATTEMPTS,
  REASONS,
  createSession,
  randomBytes,
  API_KEY_HEADER,
  SESSION_KEY_HEADER,
  getPermissionsFromSession,
  getPermissionsFromApiKey,
};
