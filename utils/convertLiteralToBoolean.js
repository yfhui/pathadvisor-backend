'use strict';

function convertLiteralToBoolean(val) {
  if (val === 'false') {
    return false;
  }

  if (val === 'true') {
    return true;
  }

  throw new Error('Argument must be string "true" or string "false"');
}

module.exports = convertLiteralToBoolean;
