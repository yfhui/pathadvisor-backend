'use strict';

const createModel = require('./base/createModel');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['_id', 'name'],
  properties: {
    _id: {
      $id: '#/properties/_id',
      type: 'string',
      minLength: 1,
    },
    name: {
      $id: '#/properties/name',
      type: 'string',
      minLength: 1,
    },
    data: {
      $id: '#/properties/data',
      type: 'object',
    },
    hasData: {
      $id: '#/properties/hasData',
      type: 'boolean',
    },
  },
};

const COLLECTION_NAME = 'tags';
const allowCustomId = true;

module.exports = createModel(COLLECTION_NAME, { schema, allowCustomId });
