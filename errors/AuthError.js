'use strict';

class AuthError extends Error {
  constructor({ username, reason }, ...params) {
    super(...params);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, AuthError);
    }

    this.reasonObject = { username, reason };
  }
}

module.exports = AuthError;
