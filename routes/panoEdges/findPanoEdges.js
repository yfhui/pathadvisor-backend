'use strict';

const panoEdges = require('../../models/panoEdges');
const successResponse = require('../../responses/successResponse');

async function findPanoEdges(req, res) {
  const { floorId } = req.params;
  const data = await (await panoEdges.find({ floorId })).toArray();

  res.json(successResponse({ data, meta: { count: data.length } }));
}

module.exports = findPanoEdges;
