'use strict';

const multer = require('multer')();
const router = require('express-promise-router')();
const findPanoImage = require('./findPanoImage');
const findPanoImageInfo = require('./findPanoImageInfo');
const uploadPanoImage = require('./uploadPanoImage');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/pano/images/:id', findPanoImage);
router.get('/pano/images/:id/info', findPanoImageInfo);
router.post(
  '/pano/images',
  checkPermissions([permissions['PANO_IMAGE:INSERT']]),
  multer.single('data'),
  uploadPanoImage,
);

module.exports = router;
