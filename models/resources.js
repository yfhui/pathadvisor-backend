'use strict';

const createModel = require('./base/createModel');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['data', 'lastUpdatedAt'],
  properties: {
    data: {
      $id: '#/properties/data',
      type: 'object',
    },
    lastUpdatedAt: {
      $id: '#/properties/lastUpdatedAt',
      type: 'integer',
    },
  },
};

const COLLECTION_NAME = 'resources';

module.exports = createModel(COLLECTION_NAME, { schema });
