'use strict';

const MAP_TILE_WIDTH = 200;
const MAP_TILE_HEIGHT = 200;
const BACKGROUND_COLOR = { r: 250, g: 250, b: 250, alpha: 1 };

const FEATURES = {
  BLACK_AND_WHITE: 'BLACK_AND_WHITE',
};

// color codes of room, stair, point of interest, etc.
const FILLED_COLORS = [
  [146, 174, 201],
  [153, 179, 204],
  [218, 218, 218],
  [207, 207, 207],
  [255, 255, 215],
  [255, 255, 213],
  [242, 239, 233],
  [242, 240, 231],
  [244, 241, 231],
  [244, 241, 230],
  [243, 240, 232],
  [240, 238, 232],
  [215, 255, 255],
  [217, 217, 217],
  [164, 191, 218],
  [235, 235, 235],
];

module.exports = {
  MAP_TILE_WIDTH,
  MAP_TILE_HEIGHT,
  BACKGROUND_COLOR,
  FEATURES,
  FILLED_COLORS,
};
