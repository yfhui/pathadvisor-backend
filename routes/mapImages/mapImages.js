'use strict';

const bodyParser = require('body-parser');
const router = require('express-promise-router')();
const findMapImage = require('./findMapImage');
const uploadMapImage = require('./uploadMapImage');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/floors/:floorId/map-image', findMapImage);
router.post(
  '/floors/:floorId/map-image',
  checkPermissions([permissions['MAP_IMAGE:UPDATE']]),
  bodyParser.raw({ limit: '2mb' }),
  uploadMapImage,
);
module.exports = router;
