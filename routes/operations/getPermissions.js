'use strict';

const permissions = require('../../auth/permissions');
const successResponse = require('../../responses/successResponse');

function getPermissions(req, res) {
  const permissionsValues = Object.values(permissions);
  res.json(successResponse({ data: permissionsValues, meta: { count: permissionsValues.length } }));
}

module.exports = getPermissions;
