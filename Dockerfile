from node:10.15.3-alpine
RUN mkdir -p /home/node/app && chown -R node:node /home/node/app
RUN apk add --no-cache --virtual deps \
  python \
  build-base
WORKDIR /home/node/app
COPY package*.json ./
USER node
RUN npm ci --only=production
COPY --chown=node:node . .
USER root
RUN apk del deps
USER node
EXPOSE 3001
CMD ["node","index.js"]

