'use strict';

const successResponse = require('../../responses/successResponse');
const suggestions = require('../../models/suggestions');

async function insertSuggestion(req, res) {
  const timestamp = new Date().getTime();
  const data = await suggestions.insertOne({
    ...req.body,
    resolved: false,
    createdAt: timestamp,
    updatedAt: timestamp,
  });
  res.json(successResponse({ data }));
}

module.exports = insertSuggestion;
