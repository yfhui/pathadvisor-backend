'use strict';

const { SESSION_KEY_HEADER, getPermissionsFromSession } = require('../../auth/auth');
const successResponse = require('../../responses/successResponse');
const AuthError = require('../../errors/AuthError');

async function isLoggedIn(req, res) {
  const sessionKey = req.header(SESSION_KEY_HEADER);
  const { error } = await getPermissionsFromSession(sessionKey);

  if (error) {
    throw new AuthError({ reason: error }, 'Failed to authorize');
  }

  res.json(successResponse({ data: { success: true } }));
}

module.exports = isLoggedIn;
