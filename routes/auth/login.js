'use strict';

const Ajv = require('ajv');

const users = require('../../models/users');
const { authenticateUser, REASONS, createSession } = require('../../auth/auth');
const successResponse = require('../../responses/successResponse');
const AuthError = require('../../errors/AuthError');
const ValidationError = require('../../errors/ValidationError');
const logger = require('../../logger/logger');

const validator = new Ajv({ allErrors: true });

async function login(req, res) {
  const { username, password } = req.body;

  const bodySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    required: ['username', 'password'],
    properties: {
      username: {
        $id: '#/properties/username',
        type: 'string',
        minLength: 1,
      },
      password: {
        $id: '#/properties/password',
        type: 'string',
        minLength: 1,
      },
    },
  };

  const validate = validator.compile(bodySchema);

  if (!validate({ username, password })) {
    throw new ValidationError(validate.errors, 'Validation Error');
  }

  const { error } = await authenticateUser(username, password);

  const timestamp = new Date().getTime();

  logger.info({
    label: 'auth',
    message: `Login in request for ${username} received`,
  });

  if (error === REASONS.INVALID_PASSWORD) {
    await users.updateOneNoValidate({ _id: username }, { $inc: { failedAttempts: 1 } });
    logger.info({
      label: 'auth',
      message: `failedAttempts for ${username} increased by 1`,
    });
  }

  if (error) {
    throw new AuthError(
      { username, reason: error },
      error === REASONS.LOCKED
        ? 'Account is locked, please contact ITSC for help'
        : 'Failed to authenticate user',
    );
  }

  const { error: sessionError, session } = await createSession(username);

  if (sessionError) {
    throw new AuthError(
      { username, reason: sessionError },
      sessionError === REASONS.MAX_SESSION_REACHED
        ? 'Account has reached the max number of sessions allowed'
        : 'Failed to authenticate user',
    );
  }

  await users.updateOne({ _id: username }, { lastLoginAt: timestamp });

  logger.info({
    label: 'auth',
    message: `login successful for user ${username} and session key has been assigned`,
  });

  res.json(successResponse({ data: { session } }));
}

module.exports = login;
