'use strict';

const sharp = require('sharp');
const SimpleImage = require('./SimpleImage');
const { FILLED_COLORS, BACKGROUND_COLOR } = require('./imageMeta');

function isSimilarColor([r, g, b]) {
  const ERROR = 3;
  const equalWithError = (x, y) => Math.abs(x - y) <= ERROR;

  return FILLED_COLORS.some(
    ([tr, tg, tb]) => equalWithError(r, tr) && equalWithError(g, tg) && equalWithError(b, tb),
  );
}

async function convertBlackAndWhiteMapImage(imageBuffer) {
  const sharpImage = sharp(imageBuffer);
  const { data, info } = await sharpImage.raw().toBuffer({ resolveWithObject: true });

  const image = new SimpleImage(data, info);
  const { width, height } = info;

  for (let x = 0; x < width; x += 1) {
    for (let y = 0; y < height; y += 1) {
      if (isSimilarColor(image.getPixel(x, y).slice(0, 3))) {
        image.setPixel(x, y, BACKGROUND_COLOR);
      }
    }
  }

  return sharp(image.buffet, { raw: info });
}

module.exports = convertBlackAndWhiteMapImage;
