'use strict';

const createModel = require('./base/createModel');
const transformNullToUnset = require('./base/transformNullToUnset');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['_id', 'floorIds'],
  properties: {
    _id: {
      $id: '#/properties/_id',
      type: 'string',
      minLength: 1,
    },
    floorIds: {
      $id: '#/properties/floorIds',
      type: 'array',
      items: {
        $id: '#/properties/floorIds/items',
        type: 'string',
        minLength: 1,
      },
    },
    weight: {
      $id: '#/properties/weight',
      type: 'number',
      exclusiveMinimum: 0,
    },
    tagIds: {
      $id: '#/properties/tagIds',
      type: 'array',
      items: {
        $id: '#/properties/tagIds/items',
        type: 'string',
        minLength: 1,
      },
    },
    ignoreMinLiftRestriction: {
      $id: '#/properties/ignoreMinLiftRestriction',
      type: 'boolean',
    },
  },
};

function transformUpdateData(data) {
  return transformNullToUnset(data);
}

function transformInsertData(data) {
  return transformUpdateData(data).set;
}

const COLLECTION_NAME = 'connectors';

module.exports = createModel(COLLECTION_NAME, { schema, transformInsertData, transformUpdateData });
