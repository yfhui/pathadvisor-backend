'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });

const keyBy = require('lodash.keyby');
const nodeModel = require('../../models/nodes');

const successResponse = require('../../responses/successResponse');
const breadthFirstSearch = require('../../graph/breadthFirstSearch');
const convertLiteralToBoolean = require('../../utils/convertLiteralToBoolean');
const ValidationError = require('../../errors/ValidationError');
const NotFoundError = require('../../errors/NotFoundError');
const transformNodeResponse = require('../nodes/transformNodeResponse');
const customSearchConditions = require('./customSearchConditions');

const { MAX_WEIGHT, MODES } = require('../../graph/constants');

const DISTANCE_UNIT = {
  MINUTE: 'MINUTE',
  METER: 'METER',
};

const PATH_NOT_FOUND_MESSAGE = 'No path found';

async function searchShortestPath(req, res) {
  const { graph, nodesById, connectorsById, settings } = req;

  if (!graph) {
    throw new Error('No graph is found');
  }

  if (!nodesById) {
    throw new Error('nodesById not found');
  }

  if (!connectorsById) {
    throw new Error('connectorsById not found');
  }

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    required: ['fromId', 'toId'],
    properties: {
      fromId: {
        $id: '#/properties/fromId',
        type: 'string',
        minLength: 1,
      },
      toId: {
        $id: '#/properties/toId',
        type: 'string',
        minLength: 1,
      },
      viaIds: {
        $id: '#/properties/viaIds',
        type: 'array',
        items: {
          $id: '#/properties/viaIds/items',
          type: 'string',
          minLength: 1,
        },
      },
      mode: {
        $id: '#/properties/mode',
        type: 'string',
        enum: Object.values(MODES),
      },
      noStairCase: {
        $id: '#/properties/noStairCase',
        type: 'string',
        enum: ['true', 'false'],
      },
      noEscalator: {
        $id: '#/properties/noEscalator',
        type: 'string',
        enum: ['true', 'false'],
      },
      stepFreeAccess: {
        $id: '#/properties/stepFreeAccess',
        type: 'string',
        enum: ['true', 'false'],
      },
    },
  };

  const validate = validator.compile(querySchema);

  const { fromId, toId, mode = MODES.SHORTEST_TIME } = req.query;
  let { noStairCase, noEscalator, stepFreeAccess, viaIds = [] } = req.query;

  if (!Array.isArray(viaIds)) {
    viaIds = [viaIds];
  }

  if (!validate({ fromId, toId, mode, noStairCase, noEscalator, viaIds, stepFreeAccess })) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  noStairCase = noStairCase ? convertLiteralToBoolean(noStairCase) : true;
  noEscalator = noEscalator ? convertLiteralToBoolean(noEscalator) : false;
  stepFreeAccess = stepFreeAccess ? convertLiteralToBoolean(stepFreeAccess) : false;

  const nodeIdsToVisit = [fromId, ...viaIds];
  const promises = [];

  function calculateDistance(node, nodeDist, nextNode, nextDist) {
    const isTravellingInLift =
      nextNode &&
      node.connectorId &&
      node.connectorId === nextNode.connectorId &&
      nodesById[node._id].tags.lift;

    let distance = nodeDist - nextDist;

    if (isTravellingInLift) {
      distance = mode === MODES.MIN_NO_OF_LIFTS ? 0 : distance * settings.minutesPerMeter;
    }

    if (distance >= MAX_WEIGHT) {
      distance = 0;
    }

    return {
      distance,
      unit: isTravellingInLift ? DISTANCE_UNIT.MINUTE : DISTANCE_UNIT.METER,
    };
  }

  nodeIdsToVisit.forEach((currPathNodeId, index) => {
    const nextPathNodeId = index !== nodeIdsToVisit.length - 1 ? nodeIdsToVisit[index + 1] : null;
    const isLastItr = index === nodeIdsToVisit.length - 1;

    if (!graph[currPathNodeId]) {
      throw new NotFoundError({}, PATH_NOT_FOUND_MESSAGE);
    }

    const { prev, dist } = breadthFirstSearch(
      graph,
      currPathNodeId,
      ({ prevNodeId, nodeId, weight, isConnectorEdge, notStepFreeEdge }) => {
        const conditions = customSearchConditions({
          node: nodesById[nodeId],
          prevNode: nodesById[prevNodeId],
          connectorsById,
          isConnectorEdge,
          notStepFreeEdge,
          mode,
          noStairCase,
          noEscalator,
          stepFreeAccess,
        });

        if (conditions) {
          return conditions;
        }

        return { weight };
      },
    );

    const pathNodeIds = isLastItr ? [toId] : [];
    let prevNodeId = isLastItr ? toId : nextPathNodeId;

    while (prev[prevNodeId]) {
      pathNodeIds.push(prev[prevNodeId]);
      prevNodeId = prev[prevNodeId];
    }

    if (prevNodeId !== currPathNodeId) {
      throw new NotFoundError({}, PATH_NOT_FOUND_MESSAGE);
    }

    promises.push(
      nodeModel
        .find(
          { _id: { $in: pathNodeIds } },
          { projection: { polygonCoordinates: 0, geoLocs: 0, keywords: 0, others: 0 } },
        )
        .then(cursor => cursor.toArray())
        .then(nodes => {
          const fullNodesById = keyBy(nodes, o => o._id);
          return pathNodeIds
            .map((nodeId, i) => {
              const node = fullNodesById[nodeId];
              const nextNodeId = i + 1 >= pathNodeIds.length ? null : pathNodeIds[i + 1];
              const nextNode = !nextNodeId ? null : fullNodesById[nextNodeId];
              const nextDist = !nextNodeId ? 0 : dist[nextNodeId];

              const { distance, unit } = calculateDistance(node, dist[nodeId], nextNode, nextDist);

              return {
                ...transformNodeResponse(req, node),
                distance,
                unit,
              };
            })
            .reverse();
        }),
    );
  });

  // flatten the array
  const data = (await Promise.all(promises)).reduce((agg, value) => {
    agg.push(...value);
    return agg;
  }, []);

  res.json(successResponse({ data }));
}

module.exports = searchShortestPath;
