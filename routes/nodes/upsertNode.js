'use strict';

const shortid = require('shortid');
const ObjectId = require('../../utils/ObjectId');
const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const nodes = require('../../models/nodes');
const connectors = require('../../models/connectors');
const floors = require('../../models/floors');
const images = require('../../models/images');
const panoImages = require('../../models/panoImages');

const ACTION = {
  INSERT: 'INSERT',
  UPDATE: 'UPDATE',
};

const upsertNode = action => async (req, res) => {
  const { connectorId, floorId, image, panoImage } = req.body;

  if (connectorId) {
    const connector = await connectors.findOne({ _id: connectorId });
    if (!connector) {
      res.status(400).json(errorResponse({ message: `connector ${connectorId} not found` }));
      return;
    }
  }

  if (floorId) {
    const floor = await floors.findOne({ _id: floorId });
    if (!floor) {
      res.status(400).json(errorResponse({ message: `floor ${floorId} not found` }));
      return;
    }
  }

  let imageId = null;
  let updateImageId = false;
  if (image) {
    imageId = new ObjectId(image);
    updateImageId = true;
    const hasImage = await images.findOne({ _id: imageId }, { projection: { _id: 1 } });
    if (!hasImage) {
      res.status(400).json(errorResponse({ message: `Image reference ${image} not found` }));
      return;
    }
  } else if (image === null) {
    updateImageId = true;
  }

  let panoImageId = null;
  let updatePanoImageId = false;
  if (panoImage) {
    panoImageId = new ObjectId(panoImage);
    updatePanoImageId = true;
    const hasPanoImage = await panoImages.findOne({ _id: panoImageId }, { projection: { _id: 1 } });
    if (!hasPanoImage) {
      res
        .status(400)
        .json(errorResponse({ message: `PanoImage reference ${panoImage} not found` }));
      return;
    }
  } else if (panoImage === null) {
    updatePanoImageId = true;
  }

  let data;
  const { image: _unused, panoImage: _unusedToo, ...sanitizedBody } = req.body;

  switch (action) {
    case ACTION.INSERT:
      data = await nodes.insertOne({
        ...sanitizedBody,
        _id: shortid(),
        ...(updateImageId ? { image: imageId } : {}),
        ...(updatePanoImageId ? { panoImage: panoImageId } : {}),
      });
      break;
    case ACTION.UPDATE:
      data = await nodes.updateOne(
        { _id: req.params.id },
        {
          ...sanitizedBody,
          ...(updateImageId ? { image: imageId } : {}),
          ...(updatePanoImageId ? { panoImage: panoImageId } : {}),
        },
      );
      break;
    default:
      throw new TypeError(`action must be one of ${Object.values(ACTION)}, ${action} received`);
  }

  res.json(successResponse({ data }));
};

module.exports = upsertNode;
