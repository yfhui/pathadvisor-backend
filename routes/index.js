'use strict';

const router = require('express-promise-router')();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoSanitize = require('express-mongo-sanitize');

const buildGraph = require('../graph/buildGraph');
const cache = require('../graph/cache');

const initDataCache = require('./initData/cache');
const initDataBuild = require('./initData/build');

const nodes = require('./nodes/nodes');
const connectors = require('./connectors/connectors');
const search = require('./search/search');
const settings = require('./settings/settings');
const floors = require('./floors/floors');
const tags = require('./tags/tags');
const edges = require('./edges/edges');
const logs = require('./logs/logs');
const suggestions = require('./suggestions/suggestions');
const auth = require('./auth/auth');
const apiKeys = require('./apiKeys/apiKeys');
const buildings = require('./buildings/buildings');
const initData = require('./initData/initData');
const mapImages = require('./mapImages/mapImages');
const mapTiles = require('./mapTiles/mapTiles');
const images = require('./images/images');
const operations = require('./operations/operations');
const resources = require('./resources/resources');
const meta = require('./meta/meta');
const panoImages = require('./panoImages/panoImages');
const panoEdges = require('./panoEdges/panoEdges');
const appVersions = require('./appVersions/appVersions');

router.use(cors());
router.options('*', cors());

router.use(bodyParser.json({ strict: false }));
router.use(mongoSanitize());

router.use(async (req, res, next) => {
  if (!initDataCache.data) {
    await initDataBuild(req);
  }

  next();
});

router.use(nodes);
router.use(connectors);
router.use(settings);
router.use(floors);
router.use(tags);
router.use(edges);
router.use(logs);
router.use(suggestions);
router.use(auth);
router.use(apiKeys);
router.use(buildings);
router.use(initData);
router.use(mapImages);
router.use(mapTiles);
router.use(images);
router.use(operations);
router.use(resources);
router.use(meta);
router.use(panoImages);
router.use(panoEdges);
router.use(appVersions);

router.use(async (req, res, next) => {
  if (!cache.data) {
    const { graph, nodesById, connectorsById, settings: settingData } = await buildGraph();
    cache.data = { graph, nodesById, connectorsById, settings: settingData };
  }

  req.graph = cache.data.graph;
  req.nodesById = cache.data.nodesById;
  req.connectorsById = cache.data.connectorsById;
  req.settings = cache.data.settings;

  next();
});
router.use(search);

module.exports = router;
