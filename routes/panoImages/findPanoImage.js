'use strict';

const panoImages = require('../../models/panoImages');
const ObjectId = require('../../utils/ObjectId');
const NotFoundError = require('../../errors/NotFoundError');

async function findPanoImage(req, res) {
  const { id } = req.params;

  const query = { _id: new ObjectId(id) };
  const data = await panoImages.findOne(query);

  if (!data) {
    throw new NotFoundError(query, 'Panorama Image not found');
  }
  if (!data.data || !data.data.buffer) {
    throw new NotFoundError(query, 'Panorama Image buffer not found in database');
  }

  res.set('Content-Type', 'image/jpeg');
  res.send(data.data.buffer);
}

module.exports = findPanoImage;
