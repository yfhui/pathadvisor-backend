'use strict';

const createModel = require('./base/createModel');
const transformNullToUnset = require('./base/transformNullToUnset');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['_id', 'floorId'],
  properties: {
    _id: {
      $id: '#/properties/_id',
      type: 'string',
      minLength: 1,
    },
    coordinates: {
      $id: '#/properties/coordinates',
      type: 'array',
      minItems: 2,
      maxItems: 2,
      items: {
        $id: '#/properties/coordinates/items',
        type: 'integer',
      },
    },
    floorId: {
      $id: '#/properties/floorId',
      type: 'string',
      minLength: 1,
    },
    tagIds: {
      $id: '#/properties/tagIds',
      type: 'array',
      items: {
        $id: '#/properties/tagIds/items',
        type: 'string',
        minLength: 1,
      },
    },
    connectorId: {
      $id: '#/properties/connectorId',
      type: 'string',
      minLength: 1,
    },
    name: {
      $id: '#/properties/name',
      type: 'string',
      minLength: 1,
    },
    keywords: {
      $id: '#/properties/keywords',
      type: 'array',
      items: {
        $id: '#/properties/keywords/items',
        type: 'string',
        minLength: 1,
      },
    },
    // mongo id ref to doc in images n
    image: {
      $id: '#/properties/image',
      type: 'object',
    },
    panoImage: {
      $id: '#/properties/panoImage',
      type: 'object',
    },
    url: {
      $id: '#/properties/url',
      type: 'string',
      minLength: 1,
    },
    polygonCoordinates: {
      $id: '#/properties/polygonCoordinates',
      type: 'array',
      minItem: 1,
      items: {
        $id: '#/properties/polygonCoordinates/items',
        type: 'array',
        minItem: 2,
        maxItems: 2,
        items: {
          $id: '#/properties/polygonCoordinates/items/value',
          type: 'integer',
        },
      },
    },
    geoLocs: {
      $id: '#/properties/geoLoc',
      type: 'object',
      additionalProperties: false,
      required: ['type', 'coordinates'],
      properties: {
        type: {
          $id: '#/properties/geoLoc/type',
          type: 'string',
          enum: ['MultiPolygon'],
        },
        coordinates: {
          $id: '#/properties/geoLoc/coordinates',
          type: 'array',
          minItems: 1,
          items: {
            $id: '#/properties/geoLoc/coordinates/rings',
            type: 'array',
            minItems: 1,
            maxItems: 2,
            items: {
              $id: '#/properties/geoLoc/coordinates/rings/points',
              type: 'array',
              minItems: 3,
              items: {
                $id: '#/properties/geoLoc/coordinates/rings/points/coordinates',
                type: 'array',
                minItems: 2,
                maxItems: 2,
                items: {
                  $id: '#/properties/geoLoc/coordinates/rings/points/coordinates/value',
                  type: 'integer',
                },
              },
            },
          },
        },
      },
    },
    unsearchable: {
      $id: '#/properties/unsearchable',
      type: 'boolean',
    },
    others: {
      $id: '#/properties/others',
      type: 'object',
    },
  },
};

function transformUpdateData(data) {
  const transformedData = { ...data };

  if (data.geoLocs) {
    transformedData.polygonCoordinates = [];
    data.geoLocs.coordinates.forEach(([outerRing = [], innerRing = []]) => {
      transformedData.polygonCoordinates.push(...outerRing, ...innerRing);
    });
  }

  return transformNullToUnset(transformedData);
}

function transformInsertData(data) {
  return transformUpdateData(data).set;
}

const COLLECTION_NAME = 'nodes';

module.exports = createModel(COLLECTION_NAME, { schema, transformInsertData, transformUpdateData });
