'use strict';

const successResponse = require('../../responses/successResponse');
const appVersions = require('../../models/appVersions');

async function findAppVersions(req, res) {
  const data = await (await appVersions.find()).toArray();
  res.json(successResponse({ data, meta: { count: data.length } }));
}

module.exports = findAppVersions;
