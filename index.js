'use strict';

const express = require('express');
const morgan = require('morgan');
const router = require('./routes/index');
const loadEnv = require('./env');
const logger = require('./logger/logger');
const ValidationError = require('./errors/ValidationError');
const NotFoundError = require('./errors/NotFoundError');
const AuthError = require('./errors/AuthError');
const errorResponse = require('./responses/errorResponse');

loadEnv();

const app = express();
const port = process.env.HTTP_PORT;

app.disable('x-powered-by');
app.enable('trust proxy');
app.use(
  morgan('combined', {
    stream: {
      write(message) {
        logger.info(message);
      },
    },
  }),
);
app.use(router);
// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  if (err instanceof ValidationError) {
    logger.warn(err);
    res.status(400).json(
      errorResponse({
        message: err.message || 'Validation error',
        validationErrors: err.validationErrors,
      }),
    );
    return;
  }

  if (err instanceof AuthError) {
    logger.warn(err);
    res.status(401).json(errorResponse({ message: err.message }));
    return;
  }

  if (err instanceof NotFoundError) {
    logger.warn(err);
    res.status(404).json(
      errorResponse({
        message: err.message,
      }),
    );
    return;
  }

  if (err.type === 'entity.parse.failed') {
    logger.warn(err);
    res.status(400).json(
      errorResponse({
        message: 'Invalid json format',
      }),
    );
    return;
  }

  if (err.type === 'entity.too.large') {
    logger.warn(err);
    res.status(400).json(
      errorResponse({
        message: 'Request entity too large',
      }),
    );
    return;
  }

  logger.error(err);
  res.status(500).json(
    errorResponse({
      message: 'Internal server error',
      stack: err.stack,
    }),
  );
});

app.use((req, res) => {
  res.status(400).json(
    errorResponse({
      message: 'Bad request',
    }),
  );
});

app.listen(port, '0.0.0.0', () => logger.info(`App listening on port ${port}!`));
