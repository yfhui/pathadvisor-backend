'use strict';

const successResponse = require('../../responses/successResponse');
const appVersions = require('../../models/appVersions');

async function insertAppVersion(req, res) {
  const timestamp = new Date().getTime();
  const data = await appVersions.insertOne({
    ...req.body,
    updatedAt: timestamp,
  });

  res.json(successResponse({ data }));
}

module.exports = insertAppVersion;
