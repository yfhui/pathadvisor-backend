'use strict';

const {
  API_KEY_HEADER,
  SESSION_KEY_HEADER,
  getPermissionsFromSession,
  getPermissionsFromApiKey,
} = require('../auth/auth');
const AuthError = require('../errors/AuthError');

const AUTH_FAIL_MESSAGE = 'Authorization failed';

function checkPermissions(requiredPermissions, queryStringKey) {
  if (!Array.isArray(requiredPermissions) || requiredPermissions.length === 0) {
    throw new Error('requiredPermissions must be an non-empty array');
  }

  return async (req, res, next) => {
    const apiKey = req.header(API_KEY_HEADER);
    const sessionKey = !queryStringKey ? req.header(SESSION_KEY_HEADER) : req.query[queryStringKey];

    switch (true) {
      case Boolean(apiKey): {
        const { error, permissions, namespace } = await getPermissionsFromApiKey(apiKey);

        if (error) {
          throw new AuthError({ reason: error }, AUTH_FAIL_MESSAGE);
        }

        if (!requiredPermissions.every(requiredPermission => permissions[requiredPermission])) {
          throw new AuthError({ reason: 'Not enough permissions' }, AUTH_FAIL_MESSAGE);
        }

        req.namespace = namespace;
        next();
        return;
      }

      case Boolean(sessionKey): {
        const { error, permissions } = await getPermissionsFromSession(sessionKey);

        if (error) {
          throw new AuthError({ reason: error }, AUTH_FAIL_MESSAGE);
        }

        if (!requiredPermissions.every(requiredPermission => permissions[requiredPermission])) {
          throw new AuthError({ reason: 'Not enough permissions' }, AUTH_FAIL_MESSAGE);
        }

        next();
        return;
      }

      default:
        throw new AuthError({ reason: 'No key provided' }, AUTH_FAIL_MESSAGE);
    }
  };
}

module.exports = checkPermissions;
