'use strict';

const successResponse = require('../../responses/successResponse');
const NotFoundError = require('../../errors/NotFoundError');
const appVersions = require('../../models/appVersions');

async function findAppVersions(req, res) {
  const data = await appVersions.findOne({ _id: req.params.id });

  if (!data) {
    throw new NotFoundError({ _id: req.params.id }, 'App version not found');
  }

  res.json(successResponse({ data }));
}

module.exports = findAppVersions;
