'use strict';

function isEmptyObject(obj) {
  return Object.keys(obj).length === 0;
}

function transformNullToUnset(data) {
  const transformedData = { ...data };

  const unset = {};

  Object.keys(transformedData).forEach(key => {
    if (transformedData[key] === null || transformedData[key] === undefined) {
      delete transformedData[key];
      unset[key] = 1;
    }
  });

  return {
    set: !isEmptyObject(transformedData) ? transformedData : null,
    unset: !isEmptyObject(unset) ? unset : null,
  };
}

module.exports = transformNullToUnset;
