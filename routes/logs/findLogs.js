'use strict';

const csvWriter = require('csv-write-stream');
const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });
const ValidationError = require('../../errors/ValidationError');
const logs = require('../../models/logs');

const headers = ['DateTime', 'mode', 'OS', 'searchFrom', 'searchTo', 'ipAddress', 'platform'];

function makeRow(item) {
  return [
    new Date(item.datetime).toISOString(),
    item.searchMode,
    item.userAgent.os.name,
    item.from,
    item.to,
    item.ipAddress,
    item.platform,
  ];
}

async function findLogs(req, res) {
  let { startDate, endDate } = req.query;
  startDate = parseInt(startDate, 10);
  endDate = parseInt(endDate, 10);

  const querySchema = {
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    required: ['startDate', 'endDate'],
    properties: {
      startDate: {
        $id: '#/properties/startDate',
        type: 'integer',
      },
      endDate: {
        $id: '#/properties/endDate',
        type: 'integer',
      },
    },
  };

  const validate = validator.compile(querySchema);

  if (!validate({ startDate, endDate })) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  const filter = {
    datetime: { $gte: startDate, $lte: endDate },
  };

  res.writeHead(200, {
    'Content-Disposition': 'attachment;filename=log.csv',
  });

  const writer = csvWriter({ headers });
  writer.pipe(res);
  const cursor = await logs.find(filter);

  while (await cursor.hasNext()) {
    const item = await cursor.next();
    writer.write(makeRow(item));
  }

  writer.end();
}

module.exports = findLogs;
