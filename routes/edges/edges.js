'use strict';

const router = require('express-promise-router')();
const findEdges = require('./findEdges');
const findEdgesWithinBox = require('./findEdgesWithinBox');
const upsertEdge = require('./upsertEdge');
const deleteEdge = require('./deleteEdge');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get(
  '/floors/:floorId/edges',
  async (req, res, next) => {
    if (Object.keys(req.query).length === 0) {
      await checkPermissions([permissions['EDGE:LIST']])(req, res, next);
      return;
    }
    next();
  },

  async (req, res) => {
    if (Object.keys(req.query).length === 0) {
      await findEdges(req, res);
      return;
    }
    await findEdgesWithinBox(req, res);
  },
);
router.post('/edges', checkPermissions([permissions['EDGE:INSERT']]), upsertEdge('INSERT'));
router.post('/edges/:id', checkPermissions([permissions['EDGE:UPDATE']]), upsertEdge('UPDATE'));
router.delete('/edges/:id', checkPermissions([permissions['EDGE:DELETE']]), deleteEdge);

module.exports = router;
