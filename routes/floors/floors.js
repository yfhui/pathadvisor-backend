'use strict';

const router = require('express-promise-router')();
const findFloors = require('./findFloors');
const findFloorById = require('./findFloorById');
const upsertFloor = require('./upsertFloor');
const deleteFloor = require('./deleteFloor');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/floors', findFloors);
router.get('/floors/:id', findFloorById);
router.post(
  '/floors/:id',
  checkPermissions([permissions['FLOOR:INSERT'], permissions['FLOOR:UPDATE']]),
  upsertFloor,
);
router.delete('/floors/:id', checkPermissions([permissions['FLOOR:DELETE']]), deleteFloor);

module.exports = router;
