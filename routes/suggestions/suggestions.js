'use strict';

const router = require('express-promise-router')();
const findSuggestions = require('./findSuggestions');
const insertSuggestion = require('./insertSuggestion');
const resolveSuggestion = require('./resolveSuggestion');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/suggestions', checkPermissions([permissions['SUGGESTION:LIST']]), findSuggestions);
router.post('/suggestions', insertSuggestion);
router.post(
  '/suggestions/:id/resolved',
  checkPermissions([permissions['SUGGESTION:UPDATE']]),
  resolveSuggestion,
);

module.exports = router;
