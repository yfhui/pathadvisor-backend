'use strict';

const shortid = require('shortid');
const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const nodes = require('../../models/nodes');
const panoEdges = require('../../models/panoEdges');

async function insertPanoEdge(req, res) {
  const { fromNodeId, toNodeId } = req.body;
  // To suppress json schema validation output to client saying missing floorId
  let floorId = 'DEFAULT';

  if (fromNodeId && toNodeId) {
    const [fromNode, toNode] = await (await nodes.find(
      {
        _id: { $in: [fromNodeId, toNodeId] },
        panoImage: { $exists: true },
      },
      { projection: { floorId: 1 } },
    )).toArray();

    if (!fromNode || !toNode || fromNode.floorId !== toNode.floorId) {
      res.status(400).json(
        errorResponse({
          message:
            'Cannot insert edge because fromNodeId or toNodeId not found, they do not contain panoramic images, or they are have different floorId',
        }),
      );
      return;
    }

    floorId = fromNode.floorId;
  }

  const _id = shortid();

  const data = await panoEdges.insertOne({ ...req.body, _id, floorId });
  res.json(successResponse({ data }));
}

module.exports = insertPanoEdge;
