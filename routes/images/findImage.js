'use strict';

const images = require('../../models/images');
const ObjectId = require('../../utils/ObjectId');
const NotFoundError = require('../../errors/NotFoundError');

async function findImage(req, res) {
  const { id } = req.params;

  const query = { _id: new ObjectId(id) };
  const data = await images.findOne(query);

  if (!data) {
    throw new NotFoundError(query, 'Image not found');
  }
  if (!data.data || !data.data.buffer) {
    throw new NotFoundError(query, 'Image buffer not found in database');
  }

  res.set('Content-Type', 'image/jpeg');
  res.send(data.data.buffer);
}

module.exports = findImage;
