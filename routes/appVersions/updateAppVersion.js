'use strict';

const successResponse = require('../../responses/successResponse');
const appVersions = require('../../models/appVersions');

async function updateAppVersion(req, res) {
  const timestamp = new Date().getTime();

  const data = await appVersions.updateOne(
    { _id: req.params.id },
    {
      ...req.body,
      updatedAt: timestamp,
    },
  );

  res.json(successResponse({ data }));
}

module.exports = updateAppVersion;
