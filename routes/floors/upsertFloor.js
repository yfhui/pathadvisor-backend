'use strict';

const successResponse = require('../../responses/successResponse');
const floors = require('../../models/floors');
const NotFoundError = require('../../errors/NotFoundError');

async function upsertFloor(req, res) {
  const _id = req.params.id;

  try {
    const data = await floors.updateOne({ _id }, req.body);
    res.json(successResponse({ data }));
  } catch (error) {
    if (!(error instanceof NotFoundError)) {
      throw error;
    }

    // insert new data if not found one
    const data = await floors.insertOne({ ...req.body, _id });
    res.json(successResponse({ data }));
  }
}

module.exports = upsertFloor;
