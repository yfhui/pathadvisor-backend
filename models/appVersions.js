'use strict';

const createModel = require('./base/createModel');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['_id', 'version', 'updatedAt'],
  properties: {
    _id: {
      $id: '#/properties/_id',
      type: 'string',
      minLength: 1,
    },
    version: {
      $id: '#/properties/version',
      type: 'string',
      minLength: 1,
    },
    updatedAt: {
      $id: '#/properties/resolvedAt',
      type: 'integer',
    },
  },
};

const COLLECTION_NAME = 'appVersions';

module.exports = createModel(COLLECTION_NAME, { schema });
