# pathadvisor-backend

Backend APIs for pathadvisor

For API documentation, please see here https://pathadvisor.ust.hk/apidocs/.

### Database dump
Please contact us to get a database dump if you wish to work on top of the latest pathadvisor data.

### Start the server in development environment
`npm run start`

### Technology stack
Express and node.js for the API server.

MongoDB for persistence layer

Native mongodb driver for node.js.

### Environment variables
Environment variables like database connection string will be loaded from `.env` file.

You can also supply environment variable during start up.

`ENV1=sample npm run start`

OS environment variables and environment variables supplied in terminal will always takes precedence than the variables in `.env` file.

There are three environment variables you need to provide.

1. `HTTP_PORT`
2. `DB_URL`
3. `NODE_ENV` (`development` or `production`)

example:
```env
DB_URL=mongodb://127.0.0.1:27017/test
HTTP_PORT=9000
NODE_ENV=development
```

### Project structure

#### index.js
Entry point

#### db/*
Database connection handlers

#### routers/*
API endpoints handlers

#### models/*
Entity abstraction. CRUD methods and schema for entities.

#### graph/*
Graph algorithm related files

#### logger/*
Logger model

#### errors
Custom error object

#### responses
Response object

Every http json output should be wrapped by `successResponse` or `errorResponse`

### Public cli scripts

Create database indexes
`npm run createIndexes`

Integration test
`npm run test`
