'use strict';

const { Binary } = require('mongodb');

const successResponse = require('../../responses/successResponse');
const tags = require('../../models/tags');

async function uploadImage(req, res) {
  const _id = req.params.id;

  await tags.updateOne({ _id }, { hasData: true, data: new Binary(req.body) });
  res.json(successResponse({ data: { success: true } }));
}

module.exports = uploadImage;
