'use strict';

const shortid = require('shortid');
const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const nodes = require('../../models/nodes');
const edges = require('../../models/edges');

const ACTION = {
  INSERT: 'INSERT',
  UPDATE: 'UPDATE',
};

const upsertEdge = action => async (req, res) => {
  const { fromNodeId, toNodeId } = req.body;
  // To suppress json schema validation output to client saying missing floorId
  let floorId = 'DEFAULT';

  if (fromNodeId && toNodeId) {
    const [fromNode, toNode] = await (await nodes.find(
      {
        _id: { $in: [fromNodeId, toNodeId] },
      },
      { projection: { floorId: 1 } },
    )).toArray();

    if (!fromNode || !toNode || fromNode.floorId !== toNode.floorId) {
      res.status(400).json(
        errorResponse({
          message:
            'Cannot insert edge because fromNodeId or toNodeId not found or they are have different floorId',
        }),
      );
      return;
    }

    floorId = fromNode.floorId;
  }

  let data;
  switch (action) {
    case ACTION.INSERT:
      data = await edges.insertOne({ ...req.body, _id: shortid(), floorId });
      break;
    case ACTION.UPDATE:
      data = await edges.updateOne({ _id: req.params.id }, { ...req.body, floorId });
      break;
    default:
      throw new TypeError(`action must be one of ${Object.values(ACTION)}, ${action} received`);
  }

  res.json(successResponse({ data }));
};

module.exports = upsertEdge;
