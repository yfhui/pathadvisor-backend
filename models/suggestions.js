'use strict';

const createModel = require('./base/createModel');

const schema = {
  definitions: {},
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['type', 'description', 'createdAt', 'updatedAt', 'resolved'],
  properties: {
    type: {
      $id: '#/properties/type',
      type: 'string',
      enum: ['new', 'wrong', 'bug', 'general'],
    },
    email: {
      $id: '#/properties/email',
      type: 'string',
      format: 'email',
    },
    name: {
      $id: '#/properties/name',
      type: 'string',
      minLength: 1,
    },
    description: {
      $id: '#/properties/description',
      type: 'string',
      minLength: 1,
    },
    floorId: {
      $id: '#/properties/floorId',
      type: 'string',
      minLength: 1,
    },
    coordinates: {
      $id: '#/properties/coordinates',
      type: 'string',
      pattern: '^-?[0-9]+,-?[0-9]+$',
    },
    createdAt: {
      $id: '#/properties/createdAt',
      type: 'integer',
    },
    updatedAt: {
      $id: '#/properties/resolvedAt',
      type: 'integer',
    },
    resolved: {
      $id: '#/properties/resolved',
      type: 'boolean',
    },
  },
  if: {
    properties: {
      type: { enum: ['new', 'wrong'] },
    },
  },
  then: { required: ['coordinates', 'floorId'] },
};

const COLLECTION_NAME = 'suggestions';

module.exports = createModel(COLLECTION_NAME, { schema });
