'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });

const nodesModel = require('../../models/nodes');
const successResponse = require('../../responses/successResponse');
const ValidationError = require('../../errors/ValidationError');
const transformNodeResponse = require('./transformNodeResponse');

async function findNodeNearCoordinates(req, res) {
  const { floorId } = req.params;
  const { nearCoordinates } = req.query;

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    required: ['floorId', 'nearCoordinates'],
    properties: {
      floorId: {
        $id: '#/properties/floorId',
        type: 'string',
        minLength: 1,
      },
      nearCoordinates: {
        $id: '#/properties/nearCoordinates',
        type: 'string',
        pattern: '^-?[0-9]+,-?[0-9]+$',
      },
    },
  };

  const validate = validator.compile(querySchema);

  if (!validate({ floorId, nearCoordinates })) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  const point = nearCoordinates.split(',').map(v => Number.parseInt(v, 10));

  // const MAX_NEAR_DISTANCE = 300;

  const node = await nodesModel.findOne(
    {
      floorId,
      coordinates: { $near: point /* , $maxDistance: MAX_NEAR_DISTANCE */ },
      panoImage: { $exists: true },
    },
    { projection: { polygonCoordinates: 0 } },
  );

  res.json(
    successResponse({
      data: node ? transformNodeResponse(req, node) : null,
    }),
  );
}

module.exports = findNodeNearCoordinates;
