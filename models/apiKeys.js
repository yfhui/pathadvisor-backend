'use strict';

const createModel = require('./base/createModel');
const permissions = require('../auth/permissions');

const schema = {
  definitions: {},
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['key', 'namespace', 'createdAt', 'updatedAt'],
  properties: {
    key: {
      $id: '#/properties/key',
      type: 'string',
      minLength: 128,
    },
    namespace: {
      $id: '#/properties/namespace',
      type: 'string',
      minLength: 1,
    },
    createdAt: {
      $id: '#/properties/createdAt',
      type: 'integer',
    },
    updatedAt: {
      $id: '#/properties/updatedAt',
      type: 'integer',
    },
    lastUsedAt: {
      $id: '#/properties/lastUsedAt',
      type: 'integer',
    },
    expiryAt: {
      $id: '#/properties/expiryAt',
      type: ['integer', 'null'],
    },
    inactive: {
      $id: '#/properties/inactive',
      type: 'boolean',
    },
    permissions: {
      $id: '#/properties/permissions',
      type: 'array',
      items: {
        type: 'string',
        enum: Object.values(permissions),
      },
    },
  },
};

const COLLECTION_NAME = 'apiKeys';

module.exports = createModel(COLLECTION_NAME, { schema });
