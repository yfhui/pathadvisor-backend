'use strict';

const createModel = require('./base/createModel');

const schema = {
  definitions: {},
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['_id', 'username'],
  properties: {
    _id: {
      $id: '#/properties/_id',
      type: 'string',
      minLength: 128,
    },
    username: {
      $id: '#/properties/username',
      type: 'string',
      minLength: 1,
    },
    lastUsedAt: {
      $id: '#/properties/lastUsedAt',
      type: 'integer',
    },
  },
};

const COLLECTION_NAME = 'sessions';

module.exports = createModel(COLLECTION_NAME, { schema });
