'use strict';

const loadEnv = require('../../../env');
const floorModel = require('../../../models/floors');
const mapImageModel = require('../../../models/mapImages');
const saveBlackAndWriteMapImage = require('../../../routes/mapImages/saveBlackAndWriteMapImage');
const { FEATURES } = require('../../../routes/mapImages/imageMeta');

loadEnv();

async function generateBlackAndWriteMapImage() {
  await mapImageModel.deleteMany({ feature: FEATURES.BLACK_AND_WHITE });

  const floors = await (await floorModel.find()).toArray();
  for (const { _id } of floors) {
    console.log(`converting floor ${_id}`);

    const [data] = await (await mapImageModel.find(
      { floorId: _id },
      {
        limit: 1,
        sort: { lastUpdatedAt: -1 },
      },
    )).toArray();

    if (!data) {
      throw new Error('Map image not found');
    }
    if (!data.data || !data.data.buffer) {
      throw new Error('Image buffer not found in database');
    }

    await saveBlackAndWriteMapImage(_id, data.data.buffer);
  }

  process.exit(0);
}

generateBlackAndWriteMapImage();
