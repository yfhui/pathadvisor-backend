'use strict';

function getResourceUrl(req, id) {
  const { API_PREFIX = `${req.protocol}://${req.get('host')}${req.baseUrl}` } = process.env;
  return `${API_PREFIX}/resources/${id}`;
}

module.exports = getResourceUrl;
