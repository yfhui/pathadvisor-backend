'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });

const { Binary } = require('mongodb');
const sharp = require('sharp');
const panoImages = require('../../models/panoImages');
const successResponse = require('../../responses/successResponse');
const getImageUrl = require('./getImageUrl');
const ValidationError = require('../../errors/ValidationError');

async function uploadPanoImage(req, res) {
  let imageBuffer = req.file.buffer;
  const { westX: westXString, name = req.file.originalname } = req.body;
  const westX = parseInt(westXString, 10);

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    required: ['westX'],
    properties: {
      westX: {
        $id: '#/properties/westX',
        type: 'integer',
      },
      name: {
        $id: '#/properties/name',
        type: 'string',
        minLength: 1,
      },
    },
  };

  const validate = validator.compile(querySchema);

  if (!validate({ westX, name })) {
    throw new ValidationError(validate.errors, 'Form fields validation errors');
  }

  if (!(imageBuffer instanceof Buffer)) {
    throw new ValidationError(
      null,
      'No image file received. Make sure you set content-type to multipart/form-data and a "data" field containing the raw image file.',
    );
  }

  const image = sharp(imageBuffer);
  const { width, height, orientation = 1 } = await image.metadata();
  const realWidth = orientation <= 4 ? width : height;
  const realHeight = orientation <= 4 ? height : width;
  if (realWidth < realHeight) {
    throw new ValidationError(
      null,
      'The width of the image is shorter than the height. Make sure you are uploading a panoramic image.',
    );
  }
  if (westX < 0 || westX > realWidth) {
    throw new ValidationError(null, 'The westX attribute is not between 0 and the image width.');
  }

  if (orientation !== 1) {
    imageBuffer = await image.rotate().toBuffer();
  }

  const lastUpdatedAt = new Date().getTime();

  const { _id } = await panoImages.insertOne({
    data: new Binary(imageBuffer),
    westX,
    lastUpdatedAt,
    ...(name ? { name } : {}),
  });
  res.json(successResponse({ data: { _id, panoImageUrl: getImageUrl(req, _id) } }));
}

module.exports = uploadPanoImage;
