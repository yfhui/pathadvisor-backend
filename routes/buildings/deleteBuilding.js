'use strict';

const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const buildings = require('../../models/buildings');
const floors = require('../../models/floors');

async function deleteBuilding(req, res) {
  const _id = req.params.id;
  const floor = await floors.findOne({ buildingId: _id });

  if (floor) {
    res.status(400).json(
      errorResponse({
        message: 'Cannot delete target building because it is connected to one or more floors',
      }),
    );
    return;
  }

  await buildings.deleteOne({ _id });
  res.json(successResponse({ meta: { deleted: true } }));
}

module.exports = deleteBuilding;
