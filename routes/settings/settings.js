'use strict';

const router = require('express-promise-router')();
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

const findSettings = require('./findSettings');
const insertSettings = require('./insertSettings');

router.get('/settings', findSettings);
router.post('/settings', checkPermissions([permissions['SETTING:INSERT']]), insertSettings);

module.exports = router;
