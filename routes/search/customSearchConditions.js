'use strict';

const { MAX_WEIGHT, CONNECTOR_SMALLEST_WEIGHT, MODES } = require('../../graph/constants');

function customSearchConditions({
  node,
  prevNode,
  connectorsById,
  isConnectorEdge,
  notStepFreeEdge,
  mode,
  noEscalator,
  noStairCase,
  stepFreeAccess,
}) {
  const { tags, connectorId } = node;
  const { connectorId: prevConnectorId } = prevNode;

  if (stepFreeAccess && notStepFreeEdge) {
    return { shouldSkip: true };
  }

  if (noEscalator && isConnectorEdge && tags && tags.escalator) {
    return { shouldSkip: true };
  }

  if (noStairCase && isConnectorEdge && tags && tags.stair) {
    return { shouldSkip: true };
  }

  if (
    mode === MODES.MIN_NO_OF_LIFTS &&
    connectorId &&
    connectorId === prevConnectorId &&
    !connectorsById[connectorId].ignoreMinLiftRestriction &&
    (tags.lift || tags.escalator || tags.crossBuildingConnector)
  ) {
    return { weight: MAX_WEIGHT };
  }

  if (mode === MODES.SHORTEST_DISTANCE && connectorId && connectorId === prevConnectorId) {
    return { weight: CONNECTOR_SMALLEST_WEIGHT };
  }

  return null;
}

module.exports = customSearchConditions;
