'use strict';

const createModel = require('./base/createModel');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['floorId', 'data', 'lastUpdatedAt'],
  properties: {
    floorId: {
      $id: '#/properties/name',
      type: 'string',
      minLength: 1,
    },
    feature: {
      $id: '#/properties/feature',
      type: 'string',
      minLength: 1,
    },
    data: {
      $id: '#/properties/data',
      type: 'object',
    },
    lastUpdatedAt: {
      $id: '#/properties/lastUpdated',
      type: 'integer',
    },
  },
};

const COLLECTION_NAME = 'mapImages';

module.exports = createModel(COLLECTION_NAME, { schema });
