'use strict';

const router = require('express-promise-router')();
const initDataCache = require('./cache');

async function initData(req, res) {
  res.header('x-in-memory-of', 'Alex Chow Tsz-lok (1997 - 2019)');
  res.json(initDataCache.data);
}

router.get('/init-data', initData);

module.exports = router;
