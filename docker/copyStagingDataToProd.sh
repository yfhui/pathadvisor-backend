#!/bin/bash

STAGING_MONGODB_CONTAINER=staging_mongo_1
PRODUCTION_MONGODB_CONTAINER=production_mongo_1
PRODUCTION_CONTAINER=production_api_1

echo "Creating prodction backup..."
docker exec ${STAGING_MONGODB_CONTAINER} sh -c "exec mongodump --db pathadvisor --excludeCollection apiKeys --excludeCollection logs  --excludeCollection sessions --excludeCollection suggestions --excludeCollection users --archive" > `date +%F_%H%M%S`.archive
echo `date +%F_%H%M%S`.archive created.

collections=(buildings connectors edges floors images mapImages mapTiles nodes settings tags users meta panoEdges panoImages)
for collection in "${collections[@]}"
do
	echo "Exporting ${collection} collection from staging database and importing to production..."
	docker exec ${STAGING_MONGODB_CONTAINER} sh -c "exec mongodump --db pathadvisor -c ${collection} --archive" | docker exec -i ${PRODUCTION_MONGODB_CONTAINER} sh -c "exec mongorestore --nsFrom '\$any\$' --nsTo '\$any\$_new' --archive"
	echo "${collection} from staging imported as ${collection}_new in production."
done


for collection in "${collections[@]}"
do
	echo "Renaming collection ${collection}_new to ${collection} and dropping exisiting"
	docker exec ${PRODUCTION_MONGODB_CONTAINER} mongo pathadvisor --eval "db.${collection}_new.renameCollection('${collection}', true)"
done

docker exec ${PRODUCTION_CONTAINER} npm run copyNodeOthers